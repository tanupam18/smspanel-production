package com.communications.tubelight.panel.sms.SmsPanel.response;

public class LoginHistoryResponse {

	private String username;
	private String ip;
	private String created;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public LoginHistoryResponse(String username, String ip, String created) {
		super();
		this.username = username;
		this.ip = ip;
		this.created = created;
	}

	public LoginHistoryResponse() {
		super();
	}

	@Override
	public String toString() {
		return "LoginHistoryResponse [username=" + username + ", ip=" + ip + ", created=" + created + "]";
	}

}
