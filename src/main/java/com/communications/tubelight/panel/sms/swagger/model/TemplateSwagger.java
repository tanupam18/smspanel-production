package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class TemplateSwagger {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TemplateSwagger(String message) {
		super();
		this.message = message;
	}

	public TemplateSwagger() {
		super();
	}

	@Override
	public String toString() {
		return "TemplateSwagger [message=" + message + "]";
	}

}
