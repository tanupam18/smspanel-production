package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UploadFIleService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public int uploadJobId() throws SQLException, ClassNotFoundException {

		Connection connection = null;
		int id = 0;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {

		}

		ResultSet resultSet;

		try {
			resultSet = statement.executeQuery("Select MAX(Jobid) +1 as jobid from UserJobs");

			if (resultSet.next()) {
				id = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("-------------------------e" + e);
		}
		connection.close();

		return id;

	}

}
