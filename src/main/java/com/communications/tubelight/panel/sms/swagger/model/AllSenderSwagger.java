package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

import java.util.Arrays;

public class AllSenderSwagger {

	private String username;
	private String[] dlrStatus;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String[] getDlrStatus() {
		return dlrStatus;
	}

	public void setDlrStatus(String[] dlrStatus) {
		this.dlrStatus = dlrStatus;
	}

	public AllSenderSwagger(String username, String[] dlrStatus) {
		super();
		this.username = username;
		this.dlrStatus = dlrStatus;
	}

	@Override
	public String toString() {
		return "AllSenderSwagger [username=" + username + ", dlrStatus=" + Arrays.toString(dlrStatus) + "]";
	}

}
