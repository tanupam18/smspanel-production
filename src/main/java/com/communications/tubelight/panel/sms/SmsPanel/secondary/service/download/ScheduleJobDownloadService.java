package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.ScheduleBulkJobResponse;

@Service
public class ScheduleJobDownloadService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public List<ScheduleBulkJobResponse> smslog(String username) {

		Connection connection = null;
		List<ScheduleBulkJobResponse> list = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		list = new ArrayList<ScheduleBulkJobResponse>();
		String status = null;
		boolean pause = false;
		boolean resume = false;
		boolean delete = false;

		String Sql;
		try {

			Sql = "Select jobid,jobstatus, CampaignName, MessageType, message, MessageLength, sender, TotalNumbers, QueuedAt, CompletedAt, TotalSent from UserJobs Where username='"
					+ username + "' AND  ScheduledAt !='0000-00-00 00:00:00'";
			;

			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<ScheduleBulkJobResponse>();
			status = null;

			while (resultSet.next()) {
				ScheduleBulkJobResponse bulkJobResponse = new ScheduleBulkJobResponse();

				bulkJobResponse.setSummary(resultSet.getLong("jobid"));
				// this.jobid = resultSet.getLong("jobid");
				bulkJobResponse.setFileName(resultSet.getString("CampaignName"));
				bulkJobResponse.setType(resultSet.getString("MessageType"));
				bulkJobResponse.setMessage(URLDecoder.decode(resultSet.getString("message")));
				bulkJobResponse.setLength(resultSet.getInt("MessageLength"));
				bulkJobResponse.setSender(resultSet.getString("sender"));
				bulkJobResponse.setTcount(resultSet.getInt("TotalNumbers"));
				bulkJobResponse.setQueuedAt(resultSet.getString("QueuedAt"));
				bulkJobResponse.setCompletedAt(resultSet.getString("CompletedAt"));
				bulkJobResponse.setTsent(resultSet.getInt("TotalSent"));
				bulkJobResponse.setStatus(resultSet.getInt("jobstatus"));

				if (resultSet.getInt("jobstatus") == 0) {
					status = "Queued";
					pause = false;
					resume = true;
					delete = true;
				} else if (resultSet.getInt("jobstatus") == 1) {
					status = "Processing";
					pause = true;
					resume = false;
					delete = true;
				} else if (resultSet.getInt("jobstatus") == 2) {
					status = "Partially Processed";
				} else if (resultSet.getInt("jobstatus") == 3) {
					status = "Completed";
				} else {
					status = "Deleted";
					pause = false;
					resume = false;
					delete = false;

				}

				bulkJobResponse.setStatusType(status);

				bulkJobResponse.setPause(pause);
				bulkJobResponse.setResume(resume);
				bulkJobResponse.setDelete(delete);

				list.add(bulkJobResponse);
			}

			resultSet.close();

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

}
