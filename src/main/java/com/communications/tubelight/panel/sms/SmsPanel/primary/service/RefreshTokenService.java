//package com.communications.tubelight.panel.sms.SmsPanel.primary.service;
//
//import java.util.Optional;
//import java.util.UUID;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.RefreshTokenEntity;
//import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.RefreshTokenRepository;
//import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepository;
//
//@Service
//@Transactional
//public class RefreshTokenService {
//
//	@Autowired
//	private RefreshTokenRepository refreshTokenRepository;
//
//	@Autowired
//	private UserRepository userRepository;
//
//	public Optional<RefreshTokenEntity> findByToken(String token) {
//		return refreshTokenRepository.findByToken(token);
//	}
//
//	public RefreshTokenEntity createRefreshToken(Long userId) {
//		RefreshTokenEntity refreshToken = new RefreshTokenEntity();
//		if (userId > 0 && userId != null)
//			deleteByUserId(userId);
//		refreshToken.setUser(userRepository.findById(userId).get());
//		//refreshToken.setToken(UUID.randomUUID().toString());
//		refreshToken.setToken(UUID.randomUUID().toString());
//
//		refreshToken = refreshTokenRepository.save(refreshToken);
//		return refreshToken;
//	}
//	
//	public Long findIdByRefreshToken(String token) {
//		return refreshTokenRepository.findIdByRefreshToken(token);
//		
//	}
//
//	public int deleteByUserId(Long userId) {
//		return refreshTokenRepository.deleteByUser(userRepository.findById(userId).get());
//	}
//
//	public String getTokenById(Long id) {
//		return refreshTokenRepository.findTokenById(id);
//	}
//
//}