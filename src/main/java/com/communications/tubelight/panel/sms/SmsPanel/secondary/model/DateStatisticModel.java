package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class DateStatisticModel {
	private String sender;
	private String fromDate;
	private int status;
	private int count;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getDate() {
		return fromDate;
	}

	public void setDate(String date) {
		this.fromDate = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public DateStatisticModel(String sender, String date, int status, int count) {
		super();
		this.sender = sender;
		this.fromDate = date;
		this.status = status;
		this.count = count;
	}

	public DateStatisticModel() {
		super();
	}

	@Override
	public String toString() {
		return "DateStatisticModel [sender=" + sender + ", date=" + fromDate + ", status=" + status + ", count=" + count
				+ "]";
	}

}
