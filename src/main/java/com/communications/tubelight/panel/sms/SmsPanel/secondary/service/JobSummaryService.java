package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.JobSummaryResponse;

@Service
public class JobSummaryService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<JobSummaryResponse> jobSummary(int offset, int noOfRecords, String search, String username,
			String source) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		List<JobSummaryResponse> list = null;
		String Sql;

		try {

			if (search != null && !search.isEmpty()) {

				if (source != null && !source.isEmpty()) {
					Sql = "Select u.QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
							+ username + "' and " + "s.JobId = u.JobId AND Source = '" + source
							+ "' AND ( u.QueuedAt like'%" + search + "%' OR u.JobId like '%" + search
							+ "%' OR Filename like '%\" + search+ \"%' OR TotalNumbers like '%" + search
							+ "%' OR Message like '%" + search + "%' )GROUP BY s.JobId ORDER BY QueuedAt limit "
							+ noOfRecords;

				} else {

					Sql = "Select u.QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
							+ username + "' and s.JobId = u.JobId  AND ( u.QueuedAt like'%" + search
							+ "%' OR u.JobId like '%" + search + "%' OR Filename like '%" + search
							+ "%' OR TotalNumbers like '%" + search + "%' OR Message like '%" + search
							+ "%' ) GROUP BY s.JobId ORDER BY QueuedAt limit " + noOfRecords;

				}

			} else {

				if (source != null) {
					Sql = "Select u.QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
							+ username + "' and  " + "s.JobId = u.JobId and Source = '" + source
							+ "' GROUP BY s.JobId ORDER BY QueuedAt limit " + "" + offset + ", " + noOfRecords;
				} else {

					Sql = "Select u.QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
							+ username + "' and " + "s.JobId = u.JobId  GROUP BY s.JobId ORDER BY QueuedAt limit " + ""
							+ offset + ", " + noOfRecords;

				}

			}

			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<JobSummaryResponse>();

			while (resultSet.next()) {
				JobSummaryResponse jobSummaryResponse = new JobSummaryResponse();

				jobSummaryResponse.setDate(resultSet.getString("queued"));
				jobSummaryResponse.setFile(resultSet.getString("Filename"));
				jobSummaryResponse.setDelivered(resultSet.getInt("Delivrd"));
				jobSummaryResponse.setJobid(resultSet.getInt("u.JobId"));
				jobSummaryResponse.setSms(resultSet.getString("Message"));
				jobSummaryResponse.setTotal(resultSet.getInt("TotalNumbers"));
				list.add(jobSummaryResponse);

			}
			resultSet.close();

			resultSet = statement.executeQuery(
					"Select QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
							+ username + "' and  \n" + "s.JobId = u.JobId  GROUP BY s.JobId ORDER BY QueuedAt DESC;");

			int count = 0;

			while (resultSet.next()) {
				count++;
			}
			this.noOfRecords = count;

		} catch (Exception e) {
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}
}
