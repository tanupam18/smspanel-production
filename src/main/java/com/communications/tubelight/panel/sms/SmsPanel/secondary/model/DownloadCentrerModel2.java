package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

import java.util.List;

public class DownloadCentrerModel2 {

	private long fromDate;
	private long toDate;
	private String mobileNo;
	private String jobId;
	private List<String> Columns;
	private String sender;
	private List<String> status;

	private int page;
	private int size;
	private String Search;

	public long getFromDate() {
		return fromDate;
	}

	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}

	public long getToDate() {
		return toDate;
	}

	public void setToDate(long toDate) {
		this.toDate = toDate;
	}

	public String getMobileNumber() {
		return mobileNo;
	}

	public void setMobileNumber(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public List<String> getColumns() {
		return Columns;
	}

	public void setColumns(List<String> columns) {
		Columns = columns;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return Search;
	}

	public void setSearch(String search) {
		Search = search;
	}

	public DownloadCentrerModel2(long fromDate, long toDate, String mobileNumber, String jobId, List<String> columns,
			String sender, List<String> status, int page, int size, String search) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		mobileNo = mobileNumber;
		this.jobId = jobId;
		Columns = columns;
		this.sender = sender;
		this.status = status;
		this.page = page;
		this.size = size;
		Search = search;
	}

	public DownloadCentrerModel2() {
		super();
	}

	@Override
	public String toString() {
		return "DownloadCentrerModel2 [fromDate=" + fromDate + ", toDate=" + toDate + ", MobileNumber=" + mobileNo
				+ ", jobId=" + jobId + ", Columns=" + Columns + ", sender=" + sender + ", status=" + status + ", page="
				+ page + ", size=" + size + ", Search=" + Search + "]";
	}

}
