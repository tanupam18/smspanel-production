package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

import java.util.Arrays;
import java.util.List;

public class SmsLogSwagger {

	private List<Data> data;
	private String[] fields;

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public SmsLogSwagger(List<Data> data, String[] fields) {
		super();
		this.data = data;
		this.fields = fields;
	}

	@Override
	public String toString() {
		return "SmsLogSwagger [data=" + data + ", fields=" + Arrays.toString(fields) + "]";
	}

}

class Data {

	private String destination;
	private String smsType;
	private String status;
	private String message;
	private String source;
	private String submitTime;
	private String deliveryTime;
	private String count;
	private String id;
	private String length;

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSmsType() {
		return smsType;
	}

	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public Data(String destination, String smsType, String status, String message, String source, String submitTime,
			String deliveryTime, String count, String id, String length) {
		super();
		this.destination = destination;
		this.smsType = smsType;
		this.status = status;
		this.message = message;
		this.source = source;
		this.submitTime = submitTime;
		this.deliveryTime = deliveryTime;
		this.count = count;
		this.id = id;
		this.length = length;
	}

	@Override
	public String toString() {
		return "Data [destination=" + destination + ", smsType=" + smsType + ", status=" + status + ", message="
				+ message + ", source=" + source + ", submitTime=" + submitTime + ", deliveryTime=" + deliveryTime
				+ ", count=" + count + ", id=" + id + ", length=" + length + "]";
	}

}
