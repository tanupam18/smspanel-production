package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.DataStatisticResponse;

@Service
public class DatewiseSummaryDownloadService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<DataStatisticResponse> datewisestatistic(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		List<DataStatisticResponse> list = null;
		String Sql;
		try {
			Sql = "Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM tube_Logs.Summary WHERE Username = '"
					+ username + "'GROUP BY Source, DlrStatus,DateTime order by DateTime ASC";

			System.out.println(Sql);
			try {
				resultSet = statement.executeQuery(Sql);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			list = new ArrayList<DataStatisticResponse>();

			try {
				while (resultSet.next()) {
					DataStatisticResponse dataStatisticResponse = new DataStatisticResponse();

					dataStatisticResponse.setDate(resultSet.getString("DateTime"));
					dataStatisticResponse.setCount(resultSet.getInt("Jcount"));
					dataStatisticResponse.setSender(resultSet.getString("Source"));
					dataStatisticResponse.setStatus(resultSet.getString("DlrStatus"));

					list.add(dataStatisticResponse);

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

		return list;

	}
}
