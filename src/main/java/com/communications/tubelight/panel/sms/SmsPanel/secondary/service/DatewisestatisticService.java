package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.DataStatisticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BulkJobResponse;

@Service
public class DatewisestatisticService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<DataStatisticResponse> datewisestatistic(int offset, int noOfRecords, String search, String username,
			String source) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		List<DataStatisticResponse> list = null;
		String Sql;
		try {

			if (search != null && !search.isEmpty()) {

				if (source != null && !source.isEmpty()) {
					Sql = "Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM tube_Logs.Summary WHERE Username = '"
							+ username + "' \n" + "and Source = '" + source + "' AND (source like '%" + search
							+ "%' OR DlrStatus like '%" + search + "%' OR DateTime like '%" + search
							+ "') GROUP BY Source, DlrStatus,DateTime order by DateTime desc limit " + noOfRecords;

				} else {

					Sql = "Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM tube_Logs.Summary WHERE Username = '"
							+ username + "'" + " AND (source like '%" + source + "%' AND DlrStatus like '%" + search
							+ "%' OR DateTime like '%" + search
							+ "' ) GROUP BY Source, DlrStatus,DateTime order by DateTime desc limit " + noOfRecords;

				}

			} else {

				if (source != null) {
					Sql = "Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM Summary WHERE Username = '"
							+ username + "'" + "and Source = '" + source
							+ "' GROUP BY Source, DlrStatus,DateTime order by DateTime desc limit " + "" + offset + ", "
							+ noOfRecords;
				} else {

					Sql = "Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM Summary WHERE Username = '"
							+ username + "'" + " GROUP BY Source, DlrStatus,DateTime order by DateTime desc limit " + ""
							+ offset + ", " + noOfRecords;
				}

			}

			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<DataStatisticResponse>();

			while (resultSet.next()) {
				DataStatisticResponse dataStatisticResponse = new DataStatisticResponse();

				dataStatisticResponse.setDate(resultSet.getString("DateTime"));
				dataStatisticResponse.setCount(resultSet.getInt("Jcount"));
				dataStatisticResponse.setSender(resultSet.getString("Source"));
				dataStatisticResponse.setStatus(resultSet.getString("DlrStatus"));

				list.add(dataStatisticResponse);

			}
			resultSet.close();

			resultSet = statement.executeQuery(
					"Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM Summary WHERE Username = '"
							+ username + "'" + " GROUP BY Source, DlrStatus,DateTime order by DateTime desc");
			System.out.println(
					"SQLCOUNT--------------Select DateTime, Source, DlrStatus, SUM(Count) as Jcount FROM Summary WHERE Username = '"
							+ username + "'" + " GROUP BY Source, DlrStatus,DateTime order by DateTime desc");

//			if (resultSet.next()) {
//				//resultSet.last();
//				this.noOfRecords = resultSet.getRow();
//
//			}
			int count = 0;
			
			while(resultSet.next()) {
				count ++;
			}
			this.noOfRecords = count;

		} catch (Exception e) {
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}
}
