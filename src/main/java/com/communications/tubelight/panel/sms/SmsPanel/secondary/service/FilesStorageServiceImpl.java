package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.communications.tubelight.panel.sms.SmsPanel.Interface.FilesStorageService;
import com.communications.tubelight.panel.sms.SmsPanel.response.BulkTemplateResponse;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {
	// private final Path root = Paths.get("uploads");
	private final Path root = Paths.get("/home/ftpjobs/");
	private String UploadFileName;
	private String proccessedUploadFileName;

	@Autowired
	TemplateBulkSerice templateBulkSerice;

//	@Override
//	public void init() {
//		try {
//			Files.createDirectory(root);
//		} catch (IOException e) {
//			throw new RuntimeException("Could not initialize folder for upload!");
//		}
//	}

	@Override
	public Map<String, Object> save(MultipartFile file, String Username) {
		// try {

		// String timestamp = new Timestamp(System.currentTimeMillis()).toString();
		// timestamp.replaceAll("", new
		// Timestamp(System.currentTimeMillis()).toString());

		DateFormat format = null;
		String formatted = null;

		Date date = new Date();
		format = new SimpleDateFormat("yyyyMMddHHmmss");
		formatted = format.format(date);

		// System.out.println("**********************************"+formatted);

		this.UploadFileName = formatted + "" + Username;
		this.proccessedUploadFileName = formatted + "new" + Username;

		int validCount = 0;
		int invalidCount = 0;

		Map<String, Object> AllDetail = new HashMap<String, Object>();
		try {
			System.out.println("---------------------" + this.root.resolve(this.UploadFileName + ".csv"));

			// Files.copy(file.getInputStream(), this.root.resolve(this.UploadFileName +
			// ".csv"));
			Files.copy(file.getInputStream(), this.root.resolve(this.UploadFileName + ".csv"));

		} catch (IOException e1) {
			System.out.println(e1);
		}

		// String FilePath = System.getProperty("user.dir") + "/uploads/" +
		// this.UploadFileName + ".csv";
		// String ProccessedFilePath = System.getProperty("user.dir") + "/uploads/" +
		// this.proccessedUploadFileName
		// + ".csv";

		String FilePath = "/home/ftpjobs/" + this.UploadFileName + ".csv";
		String ProccessedFilePath = "/home/ftpjobs/" + this.proccessedUploadFileName + ".csv";

		List<String> AllValidData = new ArrayList<String>();

		FileReader filereader = null;
		try {
			filereader = new FileReader(FilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CSVReader csvReader = new CSVReader(filereader);
		String[] nextRecord;
		List<String> AddMobileNumber = new ArrayList<String>();

		try {
			while ((nextRecord = csvReader.readNext()) != null) {
				for (String cell : nextRecord) {
					if (cell.length() == 10) {
						String MobileNo = "91" + cell;
						AddMobileNumber.add(MobileNo);

					} else {

						AddMobileNumber.add(cell);
					}

				}
			}
			System.out.println("------------------" + AddMobileNumber.size());

			HashSet<String> uniqueMobileNumber = new HashSet<String>(AddMobileNumber);
			int CountDuplicateNumber = AddMobileNumber.size() - uniqueMobileNumber.size();

			List<String> AllUniqueMobileNumber = new ArrayList<String>(uniqueMobileNumber);

			String pattern = "^(91)([6789]{1}\\d{9})$";

			for (int i = 0; i < AllUniqueMobileNumber.size(); i++) {

				java.util.regex.Pattern AllMobileNumber = java.util.regex.Pattern.compile(pattern);
				Matcher AllMobileNumberMatch = AllMobileNumber.matcher(AllUniqueMobileNumber.get(i));

				if (AllMobileNumberMatch.find() == true) {
					validCount++;
					AllValidData.add(AllUniqueMobileNumber.get(i));
				} else {
					invalidCount++;
				}
			}

			CSVWriter writer = new CSVWriter(new FileWriter(ProccessedFilePath), ',', CSVWriter.NO_QUOTE_CHARACTER);
			for (String I : AllValidData) {
				writer.writeNext(new String[] { I });
			}
			writer.close();

			AllDetail.put("valid", validCount);
			AllDetail.put("faulty", invalidCount);
			AllDetail.put("duplicate", CountDuplicateNumber);
			AllDetail.put("file", this.proccessedUploadFileName);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// } catch (Exception e) {
		/// System.out.println(e);
		// System.out.println(e.getStackTrace()[0].getLineNumber());

		// }
		return AllDetail;
	}

	@Override
	public String saveBlockNumber(MultipartFile file, String Username) {
		String FilePath = null;
		if (!file.isEmpty()) {
			this.UploadFileName = new Timestamp(System.currentTimeMillis()).toString() + "Multiple_Block_Number_"
					+ Username;

			FilePath = System.getProperty("user.dir") + "/uploads/" + this.UploadFileName + ".csv";

			try {

				try {
					Files.copy(file.getInputStream(), this.root.resolve(this.UploadFileName + ".csv"));
				} catch (IOException e1) {
					System.out.println(e1);
				}

			} catch (Exception e) {

				System.out.println(e);
			}
			try {
				Set<String> MobileNumber = null;
				FileReader filereader = null;
				filereader = new FileReader(FilePath);

				CSVReader csvReader = new CSVReader(filereader);
				String[] nextRecord;
				while ((nextRecord = csvReader.readNext()) != null) {
					MobileNumber = new HashSet<String>();

					for (String cell : nextRecord) {
						MobileNumber.add(cell);

					}
				}

				System.out.println(MobileNumber.toString());

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return FilePath;

	}

	@Override
	public String UploadFileName() {

		return this.UploadFileName;
	}

	@Override
	public Map<String, Object> personalizeSave(MultipartFile file, String Username) {

		DateFormat format = null;
		String formatted = null;

		Date date = new Date();
		format = new SimpleDateFormat("yyyyMMddHHmmss");
		formatted = format.format(date);

		this.UploadFileName = formatted + "" + Username;

		this.proccessedUploadFileName = formatted + "personalizenew" + Username;

		int validCount = 0;
		int invalidCount = 0;
		int AllValidCount = 0;

		Map<String, Object> AllDetail = new HashMap<String, Object>();
		try {
			Files.copy(file.getInputStream(), this.root.resolve(this.UploadFileName + ".csv"));

		} catch (IOException e1) {
			System.out.println(e1);
		}

//		String FilePath = System.getProperty("user.dir") + "/uploads/" + this.UploadFileName + ".csv";
//		String ProccessedFilePath = System.getProperty("user.dir") + "/uploads/" + this.proccessedUploadFileName
//				+ ".csv";

		String FilePath = "/home/ftpjobs/" + this.UploadFileName + ".csv";
		String ProccessedFilePath = "/home/ftpjobs/" + this.proccessedUploadFileName + ".csv";

		List<String> AllValidData = new ArrayList<String>();

		FileReader filereader = null;
		try {
			filereader = new FileReader(FilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CSVReader csvReader = new CSVReader(filereader, ',', '\'');
		String[] nextLine;

		try {
			List<String> AddMobilNumber = new ArrayList<String>();
			List<String> AddColumn2 = new ArrayList<String>();
			List<String> AddColumn3 = new ArrayList<String>();
			List<String> AddColumn4 = new ArrayList<String>();
			List<String> AddColumn5 = new ArrayList<String>();
			List<String> AddColumn6 = new ArrayList<String>();

			List<Map> AllColumn = new ArrayList<Map>();
			// List<String> AddMobilNumber = new ArrayList<String>();

			Map<String, String> MobileNoMap = new HashMap<String, String>();
			Map<String, String> Column2Map = new HashMap<String, String>();
			Map<String, String> Column3Map = new HashMap<String, String>();
			Map<String, String> Column4Map = new HashMap<String, String>();
			Map<String, String> Column5Map = new HashMap<String, String>();

			while ((nextLine = csvReader.readNext()) != null) {
				// System.out.println("--------NextLine Length-------" + nextLine);

				System.out.println("----------------------" + nextLine.length);
				try {
					AddMobilNumber.add(nextLine[0]);
					AddColumn2.add(nextLine[1]);
					// AddColumn3.add(nextLine[2]);
					// AddColumn4.add(nextLine[3]);
					// AddColumn5.add(nextLine[4]);

					// AddColumn6.add(nextLine[5]);
					System.out.println();

				} catch (Exception e) {
					System.out.println(e);
				}

			}
			// System.out.println("-------*******----------" + AddMobilNumber);
			// System.out.println("-------******------" + AddColumn2);
			// System.out.println("-------********------" + AddColumn3);
			// System.out.println("-------**********------" + AddColumn4);

			// HashSet<String> uniqueMobileNumber = new HashSet<String>(AddMobilNumber);
			String patterns = "^(91)([6789]{1}\\d{9})$";

			CSVWriter writer = null;
			writer = new CSVWriter(new FileWriter(ProccessedFilePath), ',', CSVWriter.NO_QUOTE_CHARACTER);
			writer.writeNext(new String[] { "Mobile", "Column1" });
			for (int j = 0; j < AddMobilNumber.size(); j++) {
				java.util.regex.Pattern AllMobileNumbers = java.util.regex.Pattern.compile(patterns);
				Matcher AllMobileNumberMatches = AllMobileNumbers.matcher(AddMobilNumber.get(j));
				if (AllMobileNumberMatches.find() == true) {

					AllValidCount++;

					// System.out.println(AddMobilNumber.get(j));
					// System.out.println(AddColumn2.get(j));
					// System.out.println(AddColumn3.get(j));

					try {

						System.out.println("AddMobilNumber.get(j)-----------" + AddMobilNumber.get(j));
						System.out.println("AddColumn2.get(j)---------------" + AddColumn2.get(j));
						writer.writeNext(new String[] { AddMobilNumber.get(j), AddColumn2.get(j) });

					} catch (Exception e) {
						System.out.println(e);
					}

				}

			}
			writer.close();

			try {

				Column2Map.put("header", AddColumn2.get(0));
				Column2Map.put("value", AddColumn2.get(1));
				AllColumn.add(Column2Map);

				Column3Map.put("header", AddColumn3.get(0));
				Column3Map.put("value", AddColumn3.get(1));
				AllColumn.add(Column3Map);

				Column4Map.put("header", AddColumn4.get(0));
				Column4Map.put("value", AddColumn4.get(1));
				AllColumn.add(Column4Map);

				Column5Map.put("header", AddColumn5.get(0));
				Column5Map.put("value", AddColumn5.get(1));

				AllColumn.add(Column5Map);

			} catch (Exception e) {
				// TODO: handle exception
			}

			// System.out.println("----------" + AddColumn2.get(0) + "-------------------");

			HashSet<String> uniqueMobileNumber = new HashSet<String>(AddMobilNumber);
			int CountDuplicateNumber = AddMobilNumber.size() - uniqueMobileNumber.size();

			List<String> AllUniqueMobileNumber = new ArrayList<String>(uniqueMobileNumber);

			String pattern = "^(91)([6789]{1}\\d{9})$";

			for (int i = 0; i < AllUniqueMobileNumber.size(); i++) {

				java.util.regex.Pattern AllMobileNumber = java.util.regex.Pattern.compile(pattern);
				Matcher AllMobileNumberMatch = AllMobileNumber.matcher(AllUniqueMobileNumber.get(i));

				if (AllMobileNumberMatch.find() == true) {
					// System.out.println("AllUnique--------------------"+AllUniqueMobileNumber.get(i)+"i------------"+i);
					// System.out.println("------AddColumn1-------------------" +
					// AddMobilNumber.get(i));
					// System.out.println("AddColumn2----------------" + AddColumn2.get(i));
					// AddColumn2.get(i);
					// System.out.println("AddColumn3----------------" + AddColumn3.get(i));

					// AddColumn3.get(i);
					// AddColumn4.get(i);

					validCount++;
					AllValidData.add(AllUniqueMobileNumber.get(i));
				} else {
					invalidCount++;
				}
			}

			AllDetail.put("valid", AllValidCount);
			AllDetail.put("faulty", invalidCount);
			AllDetail.put("duplicate", CountDuplicateNumber);
			AllDetail.put("file", this.proccessedUploadFileName);
			AllDetail.put("columns", AllColumn);

			// System.out.println(AddMobilNumber.toString());
			// writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return AllDetail;
	}

	@Override
	public String bulkUploadStorage(MultipartFile file, String Username) {

		this.UploadFileName = new Timestamp(System.currentTimeMillis()).toString() + "_" + "block_number_ " + Username;
		this.proccessedUploadFileName = new Timestamp(System.currentTimeMillis()).toString() + "_block_number_upload_"
				+ Username;

		// Map<String, Object> AllDetail = new HashMap<String, Object>();
		try {
			// Files.copy(file.getInputStream(), this.root.resolve(this.UploadFileName +
			// ".csv"));
			Path bulkUpload = Paths.get(System.getProperty("user.dir") + "/uploads/");
			Files.copy(file.getInputStream(), bulkUpload.resolve(this.UploadFileName + ".csv"));
			System.out.println("root===============" + bulkUpload.resolve(this.UploadFileName + ".csv"));
		} catch (IOException e1) {
			System.out.println(e1);
		}

		String FilePath = System.getProperty("user.dir") + "/uploads/" + this.UploadFileName + ".csv";
		System.out.println("FilePath=========================" + FilePath);

		FileReader filereader = null;
		try {
			filereader = new FileReader(FilePath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CSVReader csvReader = new CSVReader(filereader, ',', '\'');
		String[] nextLine;
		BulkTemplateResponse bulkTemplateResponse = null;

		try {
			while ((nextLine = csvReader.readNext()) != null) {
				System.out.println("1-" + nextLine[0] + "2-" + nextLine[1] + "3-" + nextLine[2] + "4-" + nextLine[3]);

				bulkTemplateResponse = new BulkTemplateResponse(nextLine[2], nextLine[1], nextLine[0], nextLine[3],
						nextLine[4]);
				System.out.println("bulkTemplateResponse==========================" + bulkTemplateResponse);
				try {
					templateBulkSerice.templateBulkService(bulkTemplateResponse, Username);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";

	}


}
