package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;

//import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.PersonalizeSmsDownloadModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@Service
public class DownlooadCsvService {

	public ByteArrayInputStream multipleLoad(final String[] residents) {
		return multipleDownloadCsvService(residents);
	}

//	public ByteArrayInputStream personalizeLoad(final String[] residents, String[] Column1, String[] Column2) {
//		return personalizeDownloadCsvService(residents, Column1, Column2);
//	}

	// write data to csv
	public ByteArrayInputStream multipleDownloadCsvService(String[] residents) {
		String[] HEADERS = { "" };
		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);
		try (final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), CSVFormat.DEFAULT)) {
			for (final String resident : residents) {
				final List<String> data = Arrays.asList(String.valueOf(resident));

				printer.printRecord(data);
			}

			printer.flush();
			return new ByteArrayInputStream(stream.toByteArray());
		} catch (final IOException e) {
			throw new RuntimeException("Csv writing error: " + e.getMessage());
		}
	}

	public ByteArrayInputStream personalizeDownloadCsvService(String[] mobileNo, String[] Column1, String[] Column2) {
		String[] HEADERS = { "" };
		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);
		try (final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), CSVFormat.DEFAULT)) {

			String[] MobileNos = { "9999999999", "8888888888", "7777777777" };
			String[] Column1s = { "aaa", "bbb", "ccc", "ddd" };
			String[] Column2s = { "xxx", "yyy", "zzz" };

			for (int i = 0; i < 3; i++) {
				// PersonalizeSmsDownloadModel personalizeSmsDownloadModel = new
				// PersonalizeSmsDownloadModel("9999999999",
				// "xxx", "aaa");

				final List<String> data = Arrays.asList(MobileNos[i], Column1s[i], Column2s[i]);
				printer.printRecord(data);

			}

			printer.flush();
			return new ByteArrayInputStream(stream.toByteArray());
		} catch (final IOException e) {
			throw new RuntimeException("Csv writing error: " + e.getMessage());
		}
	}

	public ByteArrayInputStream smsLogService(List<SmsLogResponse> list) {
		String[] HEADERS = { "SOURCE", "DESTINATION", "TYPE", "MESSAGE", "LENGTH", "COUNT", "SUBMIT TIME	",
				"DELIVERY TIME", "ID", "STATUS" };
		final CSVFormat FORMAT = CSVFormat.DEFAULT.withHeader(HEADERS);
		try (final ByteArrayOutputStream stream = new ByteArrayOutputStream();
				final CSVPrinter printer = new CSVPrinter(new PrintWriter(stream), CSVFormat.DEFAULT)) {
			for (SmsLogResponse smsLogResponse : list) {

				final List<Object> data = Arrays.asList(smsLogResponse.getSource(), smsLogResponse.getDestination(),
						smsLogResponse.getType(), smsLogResponse.getMessage(), smsLogResponse.getLength(),
						smsLogResponse.getCount(), smsLogResponse.getSubmitTime(), smsLogResponse.getDeliveryTime(),
						smsLogResponse.getId(), smsLogResponse.getStatus());
				System.out.println("SmsLogResponse--------------" + smsLogResponse.getSource());
				printer.printRecord(data);

			}
			printer.flush();
			return new ByteArrayInputStream(stream.toByteArray());

		} catch (final IOException e) {
			throw new RuntimeException("Csv writing error: " + e.getMessage());
		}

	}
	
	public ByteArrayInputStream smsLogServiceDownload(List<SmsLogResponse> list) {
		return smsLogService(list);
	}


}
