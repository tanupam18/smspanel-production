package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.LoginHistoryResponse;

@Service
public class LoginHistoryService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	private int noOfRecords;

	public List<LoginHistoryResponse> loginHistoryService(int offset, int noOfRecords, String search, String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;
		List<LoginHistoryResponse> list = null;

		if (search != null && !search.isEmpty()) {
			Sql = "SELECT user_id, user_name, ip_address, createdOn from user_login_history Where  user_name='"
					+ username + "' AND (ip_address like'%" + search + "%' OR createdOn like '%" + search
					+ "%' OR user_name like '%" + search + "%') limit " + noOfRecords;

		} else {
			Sql = "SELECT user_id, user_name, ip_address, createdOn from user_login_history where user_name= '"
					+ username + "' limit " + offset + ", " + noOfRecords;
		}

		System.out.println(Sql);

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}

		list = new ArrayList<LoginHistoryResponse>();

		try {

			while (resultSet.next()) {
				LoginHistoryResponse loginHistoryResponse = new LoginHistoryResponse();

				loginHistoryResponse.setCreated(resultSet.getString("createdOn"));
				loginHistoryResponse.setUsername(resultSet.getString("user_name"));
				loginHistoryResponse.setIp(resultSet.getString("ip_address"));

				list.add(loginHistoryResponse);

			}
			System.out.println("list--------------------------" + list);
			resultSet.close();

			resultSet = statement
					.executeQuery("SELECT count(*) from user_login_history where user_name= '" + username + "'");

			if (resultSet.next())
				this.noOfRecords = resultSet.getInt(1);

		} catch (Exception e) {
			System.out.println(e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;

	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

}
