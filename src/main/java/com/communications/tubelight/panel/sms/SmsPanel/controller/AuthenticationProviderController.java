package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.communications.tubelight.panel.sms.SmsPanel.primary.model.AuthenticationLoginRequest;
import com.communications.tubelight.panel.sms.SmsPanel.primary.service.UpdateUserByExpiryDateService;
import com.communications.tubelight.panel.sms.SmsPanel.primary.service.UserDetailsImpl;
import com.communications.tubelight.panel.sms.SmsPanel.response.AuthenticationTokenResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.ProfileResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.ProfileService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/authentication")
@CrossOrigin
@Tag(name = "1.Login API")
public class AuthenticationProviderController {

	@Autowired
	AuthenticationManager authenticationManager;

//	@Autowired
//	RefreshTokenService refreshTokenService;

	@Autowired
	UpdateUserByExpiryDateService updateuser;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ProfileService profileService;

	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody AuthenticationLoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		String role;
		if (loginRequest.getUsername().equals("durgesh")) {
			role = "admin";

		} else {
			role = "user";
		}
		// CustomerModel UserName = (CustomerModel)
		// userDetailsService.loadUserByUsername(loginRequest.getUsername());
		// System.out.println("UserName Get
		// Name----------------------"+UserName.getName());

		String token = jwtUtils.generateTokenFromUsername(loginRequest.getUsername(), 43200000, loginRequest.getIp(),
				loginRequest.getCreatedAt());
		ProfileResponse UserProfile = null;
		try {
			UserProfile = profileService.profileService(loginRequest.getUsername());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("getName---------------" + UserProfile.getName());
		// System.out.println("-----------------------"+refreshTokenEntity);
		return ResponseEntity.ok(new AuthenticationTokenResponse(token, role, loginRequest.getUsername(),
				UserProfile.getName(), UserProfile.getPrincipleEntityId()));
	}

}
