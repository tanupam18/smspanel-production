package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class BalanceSwagger {

	private String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public BalanceSwagger(String balance) {
		super();
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BalanceSwagger [balance=" + balance + "]";
	}

}
