package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.BulkJobResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.MobileLogsModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.PaginationModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.PauseRestartModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.ScheduleBulkJobResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.schedulePopupModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.BulkJobService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DeleteService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MobileLogService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.MobileLogsServices;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.PauseService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.ResumeService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.ScheduleBulkjobService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.SchedulePopupService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.SmsLogService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.changePendingSmstypeService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;
import com.communications.tubelight.panel.sms.SmsPanel.swagger.model.DownloadApi;
import com.communications.tubelight.panel.sms.SmsPanel.swagger.model.SchBulkJob;
import com.communications.tubelight.panel.sms.SmsPanel.swagger.model.BulkJobSwagger;
import com.communications.tubelight.panel.sms.SmsPanel.swagger.model.SmsLogSwagger;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.SmsLogServiceSmpp;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
@Tag(name = "5.Message Reporting API")
public class MessagingReportingController {

	@Autowired
	SmsLogService smsLogService;
	@Autowired
	BulkJobService bulkJobService;
	@Autowired
	ScheduleBulkjobService scheduleBulkjobService;
	@Autowired
	PauseService pauseService;

	@Autowired
	ResumeService resumeService;
	@Autowired
	DeleteService deleteService;
	@Autowired
	JwtUtils jwtUtils;
	@Autowired
	MobileLogService mobileLogService;
	@Autowired
	SchedulePopupService schedulePopupService;
	@Autowired
	MobileLogsServices mobileLogsServices;
	@Autowired
	changePendingSmstypeService changePendingSmstypeService;

	@Autowired
	SmsLogServiceSmpp SmsLogServiceSmpp;

	@PostMapping("/smslog")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "200 ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = SmsLogSwagger.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
			@ApiResponse(responseCode = "401", description = "unauthorized", content = @Content),
			@ApiResponse(responseCode = "405", description = "Method not Allowed", content = @Content) })
	public Map smsLogd(@RequestBody PaginationModel paginationModel, @RequestHeader Map<String, String> headers) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}

		Map<String, String> ServerInformation = changePendingSmstypeService.getCustomerPendingSms(Username);

		Map AllSmsLog = new HashMap();
		Map<String, Object> AllLebel = new HashMap<String, Object>();

		Map<String, Object> AllLebelSource = new HashMap<String, Object>();
		Map<String, Object> AllLebeldestination = new HashMap<String, Object>();
		Map<String, Object> AllLebeltype = new HashMap<String, Object>();
		Map<String, Object> AllLebelMessage = new HashMap<String, Object>();
		Map<String, Object> AllLebelLength = new HashMap<String, Object>();
		Map<String, Object> AllLebelCount = new HashMap<String, Object>();
		Map<String, Object> AllLebelSubmitTime = new HashMap<String, Object>();
		Map<String, Object> AllLebelDeliveryTime = new HashMap<String, Object>();
		Map<String, Object> AllLebelId = new HashMap<String, Object>();
		Map<String, Object> AllLebelStatus = new HashMap<String, Object>();

		SmsLogResponse logResponse = new SmsLogResponse();
		List<SmsLogResponse> SmsLogPagination = null;
		if (ServerInformation.get("server").equals("HTTP")) {

			SmsLogPagination = smsLogService.smslog((paginationModel.getPage() - 1) * paginationModel.getSize(),
					paginationModel.getSize(), paginationModel.getSearch(), Username, ServerInformation.get("logsdb"));

		} else {

			SmsLogPagination = SmsLogServiceSmpp.smslog((paginationModel.getPage() - 1) * paginationModel.getSize(),
					paginationModel.getSize(), paginationModel.getSearch(), Username, ServerInformation.get("logsdb"));

		}

		List AddAllLabel = new ArrayList();

		AllLebelSource.put("name", "source");
		AllLebelSource.put("label", "Source");

		AllLebeldestination.put("name", "destination");
		AllLebeldestination.put("label", "Destination");

		AllLebeltype.put("name", "smsType");
		AllLebeltype.put("label", "Type");

		AllLebelMessage.put("name", "message");
		AllLebelMessage.put("label", "Message");

		AllLebelLength.put("name", "length");
		AllLebelLength.put("label", "Length");

		AllLebelCount.put("name", "count");
		AllLebelCount.put("label", "Count");

		AllLebelSubmitTime.put("name", "submitTime");
		AllLebelSubmitTime.put("label", "Submit Time");

		AllLebelDeliveryTime.put("name", "deliveryTime");
		AllLebelDeliveryTime.put("label", "Delivery Time");

		AllLebelId.put("name", "id");
		AllLebelId.put("label", "ID");

		AllLebelStatus.put("name", "status");
		AllLebelStatus.put("label", "Status");

		AddAllLabel.add(AllLebelSource);
		AddAllLabel.add(AllLebeldestination);
		AddAllLabel.add(AllLebeltype);
		AddAllLabel.add(AllLebelMessage);
		AddAllLabel.add(AllLebelLength);
		AddAllLabel.add(AllLebelCount);
		AddAllLabel.add(AllLebelSubmitTime);
		AddAllLabel.add(AllLebelDeliveryTime);
		AddAllLabel.add(AllLebelId);
		AddAllLabel.add(AllLebelStatus);

		int totalCount = smsLogService.getNoOfRecords();
		System.out.println("totalCount--------------------" + totalCount);

		AllSmsLog.put("data", SmsLogPagination);
		AllSmsLog.put("totalCount", totalCount);
		AllSmsLog.put("fields", AddAllLabel);
		

		return AllSmsLog;
	}

	@PostMapping("/bulkjob")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "200 ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = BulkJobSwagger.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
			@ApiResponse(responseCode = "401", description = "unauthorized", content = @Content),
			@ApiResponse(responseCode = "405", description = "Method not Allowed", content = @Content) })
	public Map smsBulkJob(@RequestBody PaginationModel paginationModel, @RequestHeader Map<String, String> headers) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		Map<String, Object> AllLebelSummary = new HashMap<String, Object>();
		Map<String, Object> AllLebelfileName = new HashMap<String, Object>();
		Map<String, Object> AllLebeltype = new HashMap<String, Object>();
		Map<String, Object> AllLebelMessage = new HashMap<String, Object>();
		Map<String, Object> AllLebelLength = new HashMap<String, Object>();
		Map<String, Object> AllLebelTsent = new HashMap<String, Object>();
		Map<String, Object> AllLebelTCount = new HashMap<String, Object>();
		Map<String, Object> AllLebelSender = new HashMap<String, Object>();
		Map<String, Object> AllLebelQueuedAt = new HashMap<String, Object>();
		Map<String, Object> AllLebelCompleteAt = new HashMap<String, Object>();
		Map<String, Object> statusType = new HashMap<String, Object>();

		Map AllBulkSmsLog = new HashMap();

		List<BulkJobResponse> SmsLogPagination = bulkJobService.smslog(
				(paginationModel.getPage() - 1) * paginationModel.getSize(), paginationModel.getSize(),
				paginationModel.getSearch(), Username);
		int totalCount = bulkJobService.getNoOfRecords();

		List AddAllLabel = new ArrayList();

		AllLebelSummary.put("name", "summary");
		AllLebelSummary.put("label", "Summary");

		AllLebelfileName.put("name", "fileName");
		AllLebelfileName.put("label", "Campaign / File Name");

		AllLebeltype.put("name", "type");
		AllLebeltype.put("label", "Type");

		AllLebelMessage.put("name", "message");
		AllLebelMessage.put("label", "Message");

		AllLebelLength.put("name", "length");
		AllLebelLength.put("label", "Length");

		AllLebelTsent.put("name", "tsent");
		AllLebelTsent.put("label", "T.Sent");

		AllLebelTCount.put("name", "tcount");
		AllLebelTCount.put("label", "T.Count");

		AllLebelSender.put("name", "sender");
		AllLebelSender.put("label", "Sender");

		AllLebelQueuedAt.put("name", "queuedAt");
		AllLebelQueuedAt.put("label", "Queued At");

		AllLebelCompleteAt.put("name", "completedAt");
		AllLebelCompleteAt.put("label", "Completed At");

		statusType.put("name", "statusType");
		statusType.put("label", "Status");

		AddAllLabel.add(AllLebelSummary);
		AddAllLabel.add(AllLebelfileName);
		AddAllLabel.add(AllLebelMessage);
		AddAllLabel.add(AllLebelLength);
		AddAllLabel.add(AllLebelTsent);
		AddAllLabel.add(AllLebelTCount);
		AddAllLabel.add(AllLebelSender);
		AddAllLabel.add(AllLebelQueuedAt);
		AddAllLabel.add(AllLebelCompleteAt);
		AddAllLabel.add(statusType);

		AllBulkSmsLog.put("data", SmsLogPagination);
		AllBulkSmsLog.put("totalCount", totalCount);
		AllBulkSmsLog.put("fields", AddAllLabel);

		return AllBulkSmsLog;
	}

	@PostMapping("/Schedule/bulkjob")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "200 ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = SchBulkJob.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
			@ApiResponse(responseCode = "401", description = "unauthorized", content = @Content),
			@ApiResponse(responseCode = "405", description = "Method not Allowed", content = @Content) })
	public Map SchedulesmsBulkJob(@RequestBody PaginationModel paginationModel,
			@RequestHeader Map<String, String> headers) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		// Map<String, Object> AllLebel = new HashMap<String, Object>();
		Map AllBulkSmsLog = new HashMap();

		Map<String, Object> AllLebelSummary = new HashMap<String, Object>();
		Map<String, Object> AllLebelfileName = new HashMap<String, Object>();
		Map<String, Object> AllLebeltype = new HashMap<String, Object>();
		Map<String, Object> AllLebelMessage = new HashMap<String, Object>();
		Map<String, Object> AllLebelLength = new HashMap<String, Object>();
		Map<String, Object> AllLebelTsent = new HashMap<String, Object>();
		Map<String, Object> AllLebelTCount = new HashMap<String, Object>();
		Map<String, Object> AllLebelSender = new HashMap<String, Object>();
		Map<String, Object> AllLebelQueuedAt = new HashMap<String, Object>();
		Map<String, Object> AllLebelCompleteAt = new HashMap<String, Object>();
		Map<String, Object> statusType = new HashMap<String, Object>();
		Map<String, Object> ActionType = new HashMap<String, Object>();

		List<ScheduleBulkJobResponse> SmsLogPagination = scheduleBulkjobService.ScheduleBulkJob(
				(paginationModel.getPage() - 1) * paginationModel.getSize(), paginationModel.getSize(),
				paginationModel.getSearch(), paginationModel.getAction(), paginationModel.getJobid(), Username);

		int totalCount = scheduleBulkjobService.getNoOfRecords();

		List AddAllLabel = new ArrayList();

		AllLebelSummary.put("name", "summary");
		AllLebelSummary.put("label", "Summary");

		AllLebelfileName.put("name", "fileName");
		AllLebelfileName.put("label", "Campaign / File Name");

		AllLebeltype.put("name", "type");
		AllLebeltype.put("label", "Type");

		AllLebelMessage.put("name", "message");
		AllLebelMessage.put("label", "Message");

		AllLebelLength.put("name", "length");
		AllLebelLength.put("label", "Length");

		AllLebelTsent.put("name", "tsent");
		AllLebelTsent.put("label", "T.Sent");

		AllLebelTCount.put("name", "tcount");
		AllLebelTCount.put("label", "T.Count");

		AllLebelSender.put("name", "sender");
		AllLebelSender.put("label", "Sender");

		AllLebelQueuedAt.put("name", "queuedAt");
		AllLebelQueuedAt.put("label", "Queued At");

		AllLebelCompleteAt.put("name", "completedAt");
		AllLebelCompleteAt.put("label", "Completed At");

		statusType.put("name", "statusType");
		statusType.put("label", "Status");

		ActionType.put("name", "Action");
		ActionType.put("label", "Action");

		AddAllLabel.add(AllLebelSummary);
		AddAllLabel.add(AllLebelfileName);
		AddAllLabel.add(AllLebelMessage);
		AddAllLabel.add(AllLebelLength);
		AddAllLabel.add(AllLebelTsent);
		AddAllLabel.add(AllLebelTCount);
		AddAllLabel.add(AllLebelSender);
		AddAllLabel.add(AllLebelQueuedAt);
		AddAllLabel.add(AllLebelCompleteAt);
		AddAllLabel.add(statusType);
		AddAllLabel.add(ActionType);

		AllBulkSmsLog.put("data", SmsLogPagination);
		AllBulkSmsLog.put("totalCount", totalCount);
		AllBulkSmsLog.put("fields", AddAllLabel);

		return AllBulkSmsLog;
	}

	@PostMapping("/pause")
	@Hidden
	public Map<String, Object> PauseSchedule(@RequestBody PauseRestartModel pauseRestartModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		pauseService.pauseServcie(pauseRestartModel.getJobId(), Username);

		Map<String, Object> PauseOutput = new HashMap<String, Object>();
		PauseOutput.put("message", "Campaign Pause Successfully!!!");
		PauseOutput.put("value", "0");

		return PauseOutput;

	}

	@PostMapping("/resume")
	@Hidden
	public Map<String, Object> ResumeSchedule(@RequestBody PauseRestartModel pauseRestartModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		resumeService.pauseServcie(pauseRestartModel.getJobId(), Username);

		Map<String, Object> PauseOutput = new HashMap<String, Object>();
		PauseOutput.put("message", "Campaign Restart Successfully!!!");
		PauseOutput.put("value", "1");
		return PauseOutput;

	}

	@PostMapping("/delete")
	@Hidden
	public Map<String, Object> DeleteSchedule(@RequestBody PauseRestartModel pauseRestartModel,
			@RequestHeader Map<String, String> headers) {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);

		deleteService.deleteServcie(pauseRestartModel.getJobId(), Username);

		Map<String, Object> PauseOutput = new HashMap<String, Object>();
		PauseOutput.put("message", "Campaign Deleted Successfully!!!");
		PauseOutput.put("value", "4");

		return PauseOutput;

	}

	@PostMapping("/schedule/popup")
	@Hidden
	public Map<String, Object> Summarypopup(@RequestBody schedulePopupModel schedulePopupModel,
			@RequestHeader Map<String, String> headers) throws SQLException {

		String token = headers.get("authorization");
		token = token.replaceAll("Bearer ", "");

		String Username = jwtUtils.getUserNameFromJwtToken(token);
		return schedulePopupService.popupService(schedulePopupModel.getJobId());

	}

	@PostMapping("/mobile/logs")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "200 ok", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = BulkJobSwagger.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
			@ApiResponse(responseCode = "401", description = "unauthorized", content = @Content),
			@ApiResponse(responseCode = "405", description = "Method not Allowed", content = @Content) })
	public Map mobileLogs(@RequestBody MobileLogsModel mobileLogs, @RequestHeader Map<String, String> headers) {
//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		Map AllSmsLog = new HashMap();

		Map<String, Object> AllLebelSource = new HashMap<String, Object>();
		Map<String, Object> AllLebeldestination = new HashMap<String, Object>();
		Map<String, Object> AllLebeltype = new HashMap<String, Object>();
		Map<String, Object> AllLebelMessage = new HashMap<String, Object>();
		Map<String, Object> AllLebelLength = new HashMap<String, Object>();
		Map<String, Object> AllLebelCount = new HashMap<String, Object>();
		Map<String, Object> AllLebelSubmitTime = new HashMap<String, Object>();
		Map<String, Object> AllLebelDeliveryTime = new HashMap<String, Object>();
		Map<String, Object> AllLebelId = new HashMap<String, Object>();
		Map<String, Object> AllLebelStatus = new HashMap<String, Object>();

		Date fromDate = new Date(mobileLogs.getDate());
		DateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String fromDateformatted = fromDateFormat.format(fromDate);

		System.out.println(fromDateformatted);

		List<SmsLogResponse> mobileLogsData = null;
		try {
			mobileLogsData = mobileLogsServices.mobileLogs((mobileLogs.getPage() - 1) * mobileLogs.getSize(),
					mobileLogs.getSize(), mobileLogs.getSearch(), fromDateformatted, mobileLogs.getMobileNo(),
					Username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List emptyArray = new ArrayList<String>();

		AllLebelSource.put("name", "source");
		AllLebelSource.put("label", "Source");

		AllLebeldestination.put("name", "destination");
		AllLebeldestination.put("label", "Destination");

		AllLebeltype.put("name", "type");
		AllLebeltype.put("label", "Type");

		AllLebelMessage.put("name", "message");
		AllLebelMessage.put("label", "Message");

		AllLebelLength.put("name", "length");
		AllLebelLength.put("label", "Length");

		AllLebelCount.put("name", "count");
		AllLebelCount.put("label", "Count");

		AllLebelSubmitTime.put("name", "submitTime");
		AllLebelSubmitTime.put("label", "Submit Time");

		AllLebelDeliveryTime.put("name", "deliveryTime");
		AllLebelDeliveryTime.put("label", "Delivery Time");

		AllLebelId.put("name", "id");
		AllLebelId.put("label", "ID");

		AllLebelStatus.put("name", "status");
		AllLebelStatus.put("label", "Status");

		List AddAllLabel = new ArrayList();

		AddAllLabel.add(AllLebelSource);
		AddAllLabel.add(AllLebeldestination);
		AddAllLabel.add(AllLebeltype);
		AddAllLabel.add(AllLebelMessage);
		AddAllLabel.add(AllLebelLength);
		AddAllLabel.add(AllLebelCount);
		AddAllLabel.add(AllLebelSubmitTime);
		AddAllLabel.add(AllLebelDeliveryTime);
		AddAllLabel.add(AllLebelId);
		AddAllLabel.add(AllLebelStatus);

		int totalCount = mobileLogsServices.getNoOfRecords();
		if (mobileLogsData != null)
			AllSmsLog.put("data", mobileLogsData);
		else {
			AllSmsLog.put("data", emptyArray);
		}
		AllSmsLog.put("totalCount", totalCount);
		AllSmsLog.put("fields", AddAllLabel);

		return AllSmsLog;

	}
}
