package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MultipleRequestModel {

	private int page;
	private int size;
	private String search;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public MultipleRequestModel(int page, int size, String search) {
		super();
		this.page = page;
		this.size = size;
		this.search = search;
	}

	@Override
	public String toString() {
		return "MultipleRequestModel [page=" + page + ", size=" + size + ", search=" + search + "]";
	}

}
