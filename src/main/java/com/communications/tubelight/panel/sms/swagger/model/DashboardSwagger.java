package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class DashboardSwagger {

	private String submission_sms;
	private String username;
	private DlrStatus dlrStatus;

	public String getSubmission_sms() {
		return submission_sms;
	}

	public void setSubmission_sms(String submission_sms) {
		this.submission_sms = submission_sms;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public DlrStatus getDlrStatus() {
		return dlrStatus;
	}

	public void setDlrStatus(DlrStatus dlrStatus) {
		this.dlrStatus = dlrStatus;
	}

	public DashboardSwagger(String submission_sms, String username, DlrStatus dlrStatus) {
		super();
		this.submission_sms = submission_sms;
		this.username = username;
		this.dlrStatus = dlrStatus;
	}

	@Override
	public String toString() {
		return "DashboardSwagger [submission_sms=" + submission_sms + ", username=" + username + ", dlrStatus="
				+ dlrStatus + "]";
	}

}
