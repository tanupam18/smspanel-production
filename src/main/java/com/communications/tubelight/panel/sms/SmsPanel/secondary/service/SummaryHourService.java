package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;

@Service
public class SummaryHourService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public Map<String, Object> SummaryWeekMonth(String Username)
			throws ClassNotFoundException, SQLException {

		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet monthlyDashBoard;
		ResultSet resultSet2;

		monthlyDashBoard = statement.executeQuery("Select DlrStatus, Sum(Count) from Summary where Username = '"
				+ Username + "' AND DateTime = CURDATE() AND SentHour = HOUR(NOW()) group by DlrStatus");

		System.out.println("============"+"   Select DlrStatus, Sum(Count) from Summary where Username = '"
				+ Username + "' AND DateTime = CURDATE() AND SentHour = HOUR(NOW()) group by DlrStatus");
		SummaryResultSetJdbc entity = null;
		
		Map<String, Object> HourMap = new HashMap<String, Object>();
		while (monthlyDashBoard.next()) {
			String month ="";
			String DlrStatus = monthlyDashBoard.getString("DlrStatus");
			String CountDlrStatus = monthlyDashBoard.getString("Sum(Count)");
			
			HourMap.put(DlrStatus, CountDlrStatus);
			entity = new SummaryResultSetJdbc( DlrStatus, CountDlrStatus, month);

		}

		connection.close();

		return HourMap;

	}

}
