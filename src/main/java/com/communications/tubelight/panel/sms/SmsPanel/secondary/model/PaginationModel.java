package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class PaginationModel {

	private int page;
	private int size;
	private String search;

	private String action;
	private int jobid;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getJobid() {
		return jobid;
	}

	public void setJobid(int jobid) {
		this.jobid = jobid;
	}

	public PaginationModel(int page, int size, String search, String action, int jobid) {
		super();
		this.page = page;
		this.size = size;
		this.search = search;
		this.action = action;
		this.jobid = jobid;
	}

	public PaginationModel() {
		super();
	}

	@Override
	public String toString() {
		return "PaginationModel [page=" + page + ", size=" + size + ", search=" + search + ", action=" + action
				+ ", jobid=" + jobid + "]";
	}

}
