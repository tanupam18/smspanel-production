//package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;
//
//public class ColumnDownloadCentre {
//
//	private String MessageId;
//	private String Username;
//	private String Soure;
//	private boolean Destination;
//	private boolean JobId;
//	private boolean MessageLength;
//	private boolean MessageCount;
//	private boolean SentTime;
//	private boolean DeliveryTime;
//	private boolean DeliveryStatus;
//	private boolean ErrorCode;
//	private boolean Message;
//
//	public boolean isMessageId() {
//		return MessageId;
//	}
//
//	public void setMessageId(boolean messageId) {
//		MessageId = messageId;
//	}
//
//	public boolean isUsername() {
//		return Username;
//	}
//
//	public void setUsername(boolean username) {
//		Username = username;
//	}
//
//	public boolean isSoure() {
//		return Soure;
//	}
//
//	public void setSoure(boolean soure) {
//		Soure = soure;
//	}
//
//	public boolean isDestination() {
//		return Destination;
//	}
//
//	public void setDestination(boolean destination) {
//		Destination = destination;
//	}
//
//	public boolean isJobId() {
//		return JobId;
//	}
//
//	public void setJobId(boolean jobId) {
//		JobId = jobId;
//	}
//
//	public boolean isMessageLength() {
//		return MessageLength;
//	}
//
//	public void setMessageLength(boolean messageLength) {
//		MessageLength = messageLength;
//	}
//
//	public boolean isMessageCount() {
//		return MessageCount;
//	}
//
//	public void setMessageCount(boolean messageCount) {
//		MessageCount = messageCount;
//	}
//
//	public boolean isSentTime() {
//		return SentTime;
//	}
//
//	public void setSentTime(boolean sentTime) {
//		SentTime = sentTime;
//	}
//
//	public boolean isDeliveryTime() {
//		return DeliveryTime;
//	}
//
//	public void setDeliveryTime(boolean deliveryTime) {
//		DeliveryTime = deliveryTime;
//	}
//
//	public boolean isDeliveryStatus() {
//		return DeliveryStatus;
//	}
//
//	public void setDeliveryStatus(boolean deliveryStatus) {
//		DeliveryStatus = deliveryStatus;
//	}
//
//	public boolean isErrorCode() {
//		return ErrorCode;
//	}
//
//	public void setErrorCode(boolean errorCode) {
//		ErrorCode = errorCode;
//	}
//
//	public boolean isMessage() {
//		return Message;
//	}
//
//	public void setMessage(boolean message) {
//		Message = message;
//	}
//
//	public ColumnDownloadCentre(boolean messageId, boolean username, boolean soure, boolean destination, boolean jobId,
//			boolean messageLength, boolean messageCount, boolean sentTime, boolean deliveryTime, boolean deliveryStatus,
//			boolean errorCode, boolean message) {
//		super();
//		MessageId = messageId;
//		Username = username;
//		Soure = soure;
//		Destination = destination;
//		JobId = jobId;
//		MessageLength = messageLength;
//		MessageCount = messageCount;
//		SentTime = sentTime;
//		DeliveryTime = deliveryTime;
//		DeliveryStatus = deliveryStatus;
//		ErrorCode = errorCode;
//		Message = message;
//	}
//
//	public ColumnDownloadCentre() {
//		super();
//	}
//
//	@Override
//	public String toString() {
//		return "ColumnDownloadCentre [MessageId=" + MessageId + ", Username=" + Username + ", Soure=" + Soure
//				+ ", Destination=" + Destination + ", JobId=" + JobId + ", MessageLength=" + MessageLength
//				+ ", MessageCount=" + MessageCount + ", SentTime=" + SentTime + ", DeliveryTime=" + DeliveryTime
//				+ ", DeliveryStatus=" + DeliveryStatus + ", ErrorCode=" + ErrorCode + ", Message=" + Message + "]";
//	}
//
//}
