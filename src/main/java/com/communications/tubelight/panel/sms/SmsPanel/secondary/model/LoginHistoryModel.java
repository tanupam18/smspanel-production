package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class LoginHistoryModel {

	private String search;
	private int page;
	private int size;

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public LoginHistoryModel(String search, int page, int size) {
		super();
		this.search = search;
		this.page = page;
		this.size = size;
	}

	public LoginHistoryModel() {
		super();
	}

	@Override
	public String toString() {
		return "LoginHistoryModel [search=" + search + ", page=" + page + ", size=" + size + "]";
	}

}
