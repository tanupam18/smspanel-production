package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class MobileLogsModel {
	private int page;
	private int size;
	private long fromDate;
	private String mobileNo;
	private String Search;

	public long getDate() {
		return fromDate;
	}

	public void setDate(long date) {
		fromDate = date;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSearch() {
		return Search;
	}

	public void setSearch(String search) {
		Search = search;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public MobileLogsModel(int page, int size, long date, String mobileNo, String search) {
		super();
		this.page = page;
		this.size = size;
		fromDate = date;
		this.mobileNo = mobileNo;
		Search = search;
	}

	public MobileLogsModel() {
		super();
	}

	@Override
	public String toString() {
		return "MobileLogsModel [page=" + page + ", size=" + size + ", Date=" + fromDate + ", mobileNo=" + mobileNo
				+ ", Search=" + Search + "]";
	}

}
