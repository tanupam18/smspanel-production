package com.communications.tubelight.panel.sms.SmsPanel.response;

public class AllsenderResponse {

	private String sender;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public AllsenderResponse(String sender) {
		super();
		this.sender = sender;
	}

	public AllsenderResponse() {
		super();
	}

	@Override
	public String toString() {
		return "AllsenderResponse [sender=" + sender + "]";
	}

}
