package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.response.RetreiveDownloadCenterResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.DownloadCentrerModel2;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.RetreiveDownloadModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DownloadCenterRetreivingService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DownloadCentreService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

import io.swagger.v3.oas.annotations.tags.Tag;

//import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
@Tag(name="7.Download Center API")
public class DownloadCentreController {

	@Autowired
	DownloadCentreService downloadCentreService;

	@Autowired
	DownloadCenterRetreivingService downloadCenterRetreivingService;
	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/download/center")
	public Map downloadCentre(@RequestHeader Map<String, String> headers,
			@RequestBody DownloadCentrerModel2 downloadCentreModel) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		String Username =null;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			 Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}
				
		Date fromDate = new Date(downloadCentreModel.getFromDate());
		DateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		
		
		Date toDate = new Date(downloadCentreModel.getToDate());
		DateFormat toDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String fromDateformatted = fromDateFormat.format(fromDate);
		String toDateformatted = toDateFormat.format(toDate);

		String AllColumn = null;
		String AllStatus = null;

		for (int i = 0; i < downloadCentreModel.getColumns().size(); i++) {
			AllColumn = String.join(",", downloadCentreModel.getColumns().get(i));

		}
		for (int i = 0; i < downloadCentreModel.getStatus().size(); i++) {
			AllStatus = String.join(",", downloadCentreModel.getStatus().get(i));
		}

		Map<String, String> MessageResponse = new HashMap<String, String>();
		Map allData = null;

		try {
			List<RetreiveDownloadCenterResponse> a = downloadCenterRetreivingService.downloadRetreivingCenter(
					(downloadCentreModel.getPage() - 1) * downloadCentreModel.getSize(), downloadCentreModel.getSize(),
					downloadCentreModel.getSearch(), Username);
			int totalCount = downloadCenterRetreivingService.getNoOfRecords();
			
			System.out.println("Listing All Data--------------------------"+a);

			Map<String, String> DownloadIdMap = new HashMap<String, String>();
			Map<String, String> UsernameMap = new HashMap<String, String>();
			Map<String, String> SourceMap = new HashMap<String, String>();
			Map<String, String> FromMap = new HashMap<String, String>();
			Map<String, String> ToMap = new HashMap<String, String>();
			Map<String, String> DataCountMap = new HashMap<String, String>();
			Map<String, String> RequestTimeMap = new HashMap<String, String>();
			Map<String, String> StatusMap = new HashMap<String, String>();

			DownloadIdMap.put("name", "downloadId");
			DownloadIdMap.put("label", "Download Id");

			UsernameMap.put("name", "username");
			UsernameMap.put("label", "Username");

			SourceMap.put("name", "source");
			SourceMap.put("label", "Source");

			FromMap.put("name", "fromDate");
			FromMap.put("label", "From");

			ToMap.put("name", "toDate");
			ToMap.put("label", "To");

			DataCountMap.put("name", "dataCount");
			DataCountMap.put("label", "Data Count");

			RequestTimeMap.put("name", "requestTime");
			RequestTimeMap.put("label", "Request Time");

			StatusMap.put("name", "status");
			StatusMap.put("label", "Status");

			List AddAllLabel = new ArrayList<String>();

			AddAllLabel.add(DownloadIdMap);
			AddAllLabel.add(UsernameMap);
			AddAllLabel.add(SourceMap);
			AddAllLabel.add(FromMap);
			AddAllLabel.add(ToMap);
			AddAllLabel.add(DataCountMap);
			AddAllLabel.add(RequestTimeMap);
			AddAllLabel.add(StatusMap);

			allData = new HashMap();
			allData.put("data", a);
			allData.put("totalCount", totalCount);
			allData.put("fields", AddAllLabel);

			allData.put("message", "Your log request has been queued successfully, check download section for report.");

			System.out.println("fromDate------------------" + downloadCentreModel.getFromDate());
			if (downloadCentreModel.getFromDate() != 0 && downloadCentreModel.getToDate() != 0) {
				System.out.println("FromDate-------" + downloadCentreModel.getFromDate() + " toDate---------"
						+ downloadCentreModel.getToDate());
				downloadCentreService.downloadService(Username, downloadCentreModel.getSender(),
						downloadCentreModel.getMobileNumber(), fromDateformatted, toDateformatted,
						downloadCentreModel.getJobId(), AllColumn, AllStatus);

			}
			MessageResponse.put("message",
					"Your log request has been queued successfully, check download section for report.");

		} catch (Exception e) {
			MessageResponse.put("message", "Please contact to support team");

		}

		return allData;

	}

	@PostMapping("/retreive/download/center")
	public Map retreiveDownloadCenter(@RequestBody RetreiveDownloadModel retreiveDownloadModel,
			@RequestHeader Map<String, String> headers) throws SQLException {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			 Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}

		List<RetreiveDownloadCenterResponse> a = downloadCenterRetreivingService.downloadRetreivingCenter(
				(retreiveDownloadModel.getPage() - 1) * retreiveDownloadModel.getSize(),
				retreiveDownloadModel.getSize(), retreiveDownloadModel.getSearch(), Username);
		int totalCount = downloadCenterRetreivingService.getNoOfRecords();

		Map<String, String> DownloadIdMap = new HashMap<String, String>();
		Map<String, String> UsernameMap = new HashMap<String, String>();
		Map<String, String> SourceMap = new HashMap<String, String>();
		Map<String, String> FromMap = new HashMap<String, String>();
		Map<String, String> ToMap = new HashMap<String, String>();
		Map<String, String> DataCountMap = new HashMap<String, String>();
		Map<String, String> RequestTimeMap = new HashMap<String, String>();
		Map<String, String> StatusMap = new HashMap<String, String>();

		DownloadIdMap.put("name", "downloadId");
		DownloadIdMap.put("label", "Download Id");

		UsernameMap.put("name", "username");
		UsernameMap.put("label", "Username");

		SourceMap.put("name", "source");
		SourceMap.put("label", "Source");

		FromMap.put("name", "fromDate");
		FromMap.put("label", "From");

		ToMap.put("name", "toDate");
		ToMap.put("label", "To");

		DataCountMap.put("name", "dataCount");
		DataCountMap.put("label", "Data Count");

		RequestTimeMap.put("name", "requestTime");
		RequestTimeMap.put("label", "Request Time");

		StatusMap.put("name", "status");
		StatusMap.put("label", "Status");

		List AddAllLabel = new ArrayList<String>();

		AddAllLabel.add(DownloadIdMap);
		AddAllLabel.add(UsernameMap);
		AddAllLabel.add(SourceMap);
		AddAllLabel.add(FromMap);
		AddAllLabel.add(ToMap);
		AddAllLabel.add(DataCountMap);
		AddAllLabel.add(RequestTimeMap);
		AddAllLabel.add(StatusMap);

		Map allData = new HashMap();
		allData.put("data", a);
		allData.put("totalCount", totalCount);
		allData.put("fields", AddAllLabel);

		return allData;

	}

}
