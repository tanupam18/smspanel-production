package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.MonthlyStatistic;

@Service
public class MonthwiseSummaryDownloadService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<MonthlyStatistic> monthwiseStatistic(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;

		List<MonthlyStatistic> list = null;
		String Sql;

		try {

			Sql = "SELECT count(*) AS Count,DATE_FORMAT(`DateTime`,'%b%Y') AS SDate,SUM(Count) AS Submission, \n"
					+ "sum(if(DlrStatus = 'DELIVRD', Count, 0)) AS 'DELIVRD', sum(if(DlrStatus ='UNDELIV', Count, 0)) AS 'UNDELIV', \n"
					+ "sum(if(DlrStatus = 'EXPIRED', Count, 0)) AS 'EXPIRED', sum(if(DlrStatus = 'REJECTD', Count, 0)) AS 'REJECTD', \n"
					+ "sum(if(DlrStatus = 'DND', Count, 0)) AS 'DND',sum(if(DlrStatus = 'PENDING', Count, 0)) AS 'PENDING',\n"
					+ "(SUM(Count)- (sum(if(DlrStatus = 'DELIVRD', Count, 0)) + sum(if(DlrStatus ='UNDELIV', Count, 0)) + \n"
					+ "sum(if(DlrStatus = 'EXPIRED', Count, 0)) + sum(if(DlrStatus = 'DND', Count, 0)) + \n"
					+ "sum(if(DlrStatus = 'REJECTD', Count, 0)) + sum(if(DlrStatus = 'PENDING', Count, 0)))) as Other, \n"
					+ "SUM(Price) AS Price FROM Summary where Username='" + username
					+ "' GROUP BY month(`DateTime`),year(`DateTime`) ORDER BY Date(`DateTime`) ASC";

			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<MonthlyStatistic>();

			while (resultSet.next()) {
				MonthlyStatistic dataStatisticResponse = new MonthlyStatistic();
				dataStatisticResponse.setDate(resultSet.getString("SDate"));
				dataStatisticResponse.setDelivered(resultSet.getInt("DELIVRD"));
				dataStatisticResponse.setDnd(resultSet.getInt("DND"));
				dataStatisticResponse.setExpired(resultSet.getInt("EXPIRED"));
				dataStatisticResponse.setPending(resultSet.getInt("PENDING"));
				dataStatisticResponse.setUndelivered(resultSet.getInt("UNDELIV"));
				dataStatisticResponse.setOther(resultSet.getInt("Other"));
				dataStatisticResponse.setRejected(resultSet.getInt("REJECTD"));
				dataStatisticResponse.setSubmission(resultSet.getInt("Submission"));
				list.add(dataStatisticResponse);

			}
			resultSet.close();

		} catch (Exception e) {
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;

	}
}
