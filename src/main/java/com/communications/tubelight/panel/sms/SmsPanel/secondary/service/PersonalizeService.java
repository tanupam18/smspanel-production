package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PersonalizeService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	private int id;

	public void personalizeService(String username, String message, int msgType, int smsLength, String sender,
			String formatted, String fileName, int totalnumber, String metadata) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String InsertSql = null;

		if (formatted != null && !formatted.isEmpty()) {
			try {
				InsertSql = "INSERT INTO UserJobs(MetaData, TotalNumbers,Username, Message, MessageType, MessageLength, Sender, JobType, ScheduledAt) "
						+ "VALUES ( '" + metadata + "'," + totalnumber + ",'" + username + "', '"
						+ URLEncoder.encode(message, "UTF-8") + "', '" + msgType + "','" + smsLength + "', '" + sender
						+ "', '" + 2 + "', '" + formatted + "')";
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			try {
				InsertSql = "INSERT INTO UserJobs(MetaData, TotalNumbers, Username, Message, MessageType, MessageLength, Sender, JobType) "
						+ "VALUES ('" + metadata + "'," + totalnumber + ",'" + username + "', '"
						+ URLEncoder.encode(message, "UTF-8") + "', '" + msgType + "','" + smsLength + "', '" + sender
						+ "', '" + 2 + "')";
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("InsertSQl------------------------" + InsertSql);

		try {
			statement.executeUpdate(InsertSql, Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet resultSet;
		try {
			// resultSet = statement.executeQuery("select last_insert_id()");

			resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				this.id = resultSet.getInt(1);
			}

			File f = new File("/home/ftpjobs/" + fileName + ".csv");

			if (f.exists()) {
				File rename = new File("/home/ftpjobs/" + this.id + ".csv");
				System.out.println(f.renameTo(rename));

				System.out.println("Exist");
			} else {
				System.out.println("Not Exist");
			}

		} catch (SQLException e) {
			System.out.println(e);
		}

		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
