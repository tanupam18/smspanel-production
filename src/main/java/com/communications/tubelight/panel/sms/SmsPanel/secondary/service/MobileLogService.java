package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;

@Service
public class MobileLogService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<SmsLogResponse> smslog(int offset, int noOfRecords, String search) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet;
		List<SmsLogResponse> list = null;

		String Sql;
		try {
			if (search != null) {

				Sql = "Select source, destination, msgtype, Message, msglength, msgcount,uuid, sentstatus,smscSubmitdate, SmscDonedate from tube_Logs.PendingSms Where destination like '%"
						+ search + "%' OR MsgLength like '%" + search + "%' OR msgcount like '%" + search
						+ "%' OR uuid like '%" + search + "%' OR SentStatus like '%" + search
						+ "%' OR SmscSubmitDate like '%" + search + "%' OR SmscDoneDate like '%" + search
						+ "%' OR Message like '%" + search + "%' OR source like '%" + search + "%' limit "
						+ noOfRecords;

			} else {
				Sql = "Select source, destination, msgtype, Message, msglength, msgcount,uuid, sentstatus,smscSubmitdate, SmscDonedate from PendingSms limit "
						+ offset + ", " + noOfRecords;
			}

			System.out.println(Sql);
			resultSet = statement.executeQuery(Sql);

			list = new ArrayList<SmsLogResponse>();

			while (resultSet.next()) {

				SmsLogResponse logResponse = new SmsLogResponse();
				logResponse.setSource(resultSet.getString("source"));
				logResponse.setDestination(resultSet.getString("destination"));
				logResponse.setType(resultSet.getInt("msgtype"));
				logResponse.setMessage(resultSet.getString("Message"));
				logResponse.setLength(resultSet.getInt("msglength"));
				logResponse.setCount(resultSet.getInt("msgcount"));
				logResponse.setId(resultSet.getString("uuid"));
				logResponse.setStatus(resultSet.getString("sentstatus"));
				logResponse.setSubmitTime(resultSet.getString("smscSubmitdate"));
				logResponse.setDeliveryTime(resultSet.getString("SmscDonedate"));

				list.add(logResponse);
			}

			resultSet.close();

			resultSet = statement.executeQuery("Select count(*) from PendingSms");

			if (resultSet.next())
				this.noOfRecords = resultSet.getInt(1);
			

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

}
