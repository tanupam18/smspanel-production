package com.communications.tubelight.panel.sms.SmsPanel.primary.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class SmsCountService {
	
	private static final Set<String> GSM7BITEXT = new HashSet<String>(Arrays.asList(
            new String[]{
                "\f", "^", "{", "}", "\\", "[", "~", "]", "|", "€"
            }
    ));
	
	public int getSmsCount(int type, int smsLength) {
        int count = 1;
        if (type == 0 || type == 2) {
            if (smsLength > 160) {
                if ((smsLength % 153) == 0) {
                    return (int) Math.ceil(smsLength / 153);
                }
                return (int) Math.ceil(smsLength / 153) + 1;
            }
        } else if (type == 1 || type == 3) {
            if (smsLength > 70) {
                if ((smsLength % 67) == 0) {
                    return (int) Math.ceil(smsLength / 67);
                }
                return (int) Math.ceil(smsLength / 67) + 1;
            }
        }
        return count;
    }
	
	 public static int getSmsLength(String content) {

	        StringBuilder content7bit = new StringBuilder();

	        // Add escape characters for extended charset
	        for (int i = 0; i < content.length(); i++) {
	            if (!GSM7BITEXT.contains(content.charAt(i) + "")) {
	                content7bit.append(content.charAt(i));
	            } else {
	                content7bit.append('\u001b');
	                content7bit.append(content.charAt(i));
	            }
	        }

	        return content7bit.length();

	    }

}
