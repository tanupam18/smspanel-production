package com.communications.tubelight.panel.sms.SmsPanel.primary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;
import com.communications.tubelight.panel.sms.SmsPanel.response.AllsenderResponse;

@Service
public class AllSenderService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public ArrayList<AllsenderResponse> allSender(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ResultSet resultSet;
		Map<String, Object> AllSender1 = null;
		ArrayList<AllsenderResponse> allSenderResponseList = new ArrayList<>();

		try {
			resultSet = statement.executeQuery(
					"Select distinct(sender) as sender from UsersSenderId where Username='" + username + "'");

			String sender;

			while (resultSet.next()) {

				sender = resultSet.getString("sender").trim();

				AllsenderResponse allsenderResponse = new AllsenderResponse(sender);
				allSenderResponseList.add(allsenderResponse);

			}
		} catch (SQLException e) {

		}
		try {
			connection.close();
		} catch (SQLException e) {
		}

		return allSenderResponseList;
	}

}
