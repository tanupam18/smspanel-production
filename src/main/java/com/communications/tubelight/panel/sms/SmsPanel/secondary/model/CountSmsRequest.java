package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class CountSmsRequest {

	private String sender;
	private long fromDate;
	private long toDate;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public long getFromDate() {
		return fromDate;
	}

	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}

	public long getToDate() {
		return toDate;
	}

	public void setToDate(long toDate) {
		this.toDate = toDate;
	}

	public CountSmsRequest(String sender, long fromDate, long toDate) {
		super();
		this.sender = sender;
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public CountSmsRequest() {
		super();
	}

	@Override
	public String toString() {
		return "CountSmsRequest [sender=" + sender + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}

}

