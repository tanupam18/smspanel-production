package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

import java.util.Arrays;
import java.util.List;

public class BulkJobSwagger {

	private List<DataBulkJob> data;
	private String totalCount;
	private String[] fields;
	
	

	public String[] getFeilds() {
		return fields;
	}

	public void setFeilds(String[] feilds) {
		this.fields = feilds;
	}

	public List<DataBulkJob> getData() {
		return data;
	}

	public void setData(List<DataBulkJob> data) {
		this.data = data;
	}

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	
	public BulkJobSwagger(List<DataBulkJob> data, String totalCount, String[] feilds) {
		super();
		this.data = data;
		this.totalCount = totalCount;
		this.fields = feilds;
	}

	@Override
	public String toString() {
		return "BulkJobSwagger [data=" + data + ", totalCount=" + totalCount + ", feilds=" + Arrays.toString(fields)
				+ "]";
	}

	

}

class DataBulkJob {

	private String summary;
	private String fileName;
	private String message;
	private String length;
	private String tsent;
	private String tcount;
	private String sender;
	private String queuedAt;
	private String completedAt;
	private String statusType;

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getTsent() {
		return tsent;
	}

	public void setTsent(String tsent) {
		this.tsent = tsent;
	}

	public String getTcount() {
		return tcount;
	}

	public void setTcount(String tcount) {
		this.tcount = tcount;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getQueuedAt() {
		return queuedAt;
	}

	public void setQueuedAt(String queuedAt) {
		this.queuedAt = queuedAt;
	}

	public String getCompletedAt() {
		return completedAt;
	}

	public void setCompletedAt(String completedAt) {
		this.completedAt = completedAt;
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public DataBulkJob(String summary, String fileName, String message, String length, String tsent, String tcount,
			String sender, String queuedAt, String completedAt, String statusType) {
		super();
		this.summary = summary;
		this.fileName = fileName;
		this.message = message;
		this.length = length;
		this.tsent = tsent;
		this.tcount = tcount;
		this.sender = sender;
		this.queuedAt = queuedAt;
		this.completedAt = completedAt;
		this.statusType = statusType;
	}

	@Override
	public String toString() {
		return "DataBulkJob [summary=" + summary + ", fileName=" + fileName + ", message=" + message + ", length="
				+ length + ", tsent=" + tsent + ", tcount=" + tcount + ", sender=" + sender + ", queuedAt=" + queuedAt
				+ ", completedAt=" + completedAt + ", statusType=" + statusType + "]";
	}

}
