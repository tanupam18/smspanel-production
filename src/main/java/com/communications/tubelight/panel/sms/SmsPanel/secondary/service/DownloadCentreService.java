package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DownloadCentreService {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public void downloadService(String Username, String Sender, String mobileno, String fromDate, String toDate,
			String jobId, String addColumn, String addStatus) throws SQLException {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String RandomUuid = UUID.randomUUID().toString();
		String timestamp = new Timestamp(System.currentTimeMillis()).toString();

		String Sql;
		Sql = "INSERT INTO `Main`.`DownloadLogs` (`UUID`, `Username`, `Sender`, `SearchStatus`, `ColumnsToSelect`, `Mobile`, `From`, `To`, `JobId`, `DataCount`, `Status`, `ProcessServer`, `RequestTime`) "
				+ "VALUES ('" + RandomUuid + "', '" + Username + "', '" + Sender + "', '"+addStatus+"', '"
				+ addColumn + "', '" + mobileno + "', '" + fromDate + "', '" + toDate + "', '" + jobId
				+ "', '0', '0', 'SMSC', '" + timestamp + "')";

		statement.executeUpdate(Sql);
		connection.close();

	}

	

}
