package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class ProfileModel {
	private String companyName;
	private String ContactNo;
	private String principleEntityId;
	private String Address;
	private String Name;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactNo() {
		return ContactNo;
	}

	public void setContactNo(String contactNo) {
		ContactNo = contactNo;
	}

	public String getPrincipleEntityId() {
		return principleEntityId;
	}

	public void setPrincipleEntityId(String principleEntityId) {
		this.principleEntityId = principleEntityId;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public ProfileModel(String companyName, String contactNo, String principleEntityId, String address,
			String name) {
		super();
		this.companyName = companyName;
		ContactNo = contactNo;
		this.principleEntityId = principleEntityId;
		Address = address;
		Name = name;
	}

	public ProfileModel() {
		super();
	}

	@Override
	public String toString() {
		return "ProfileResponse [companyName=" + companyName + ", ContactNo=" + ContactNo + ", principleEntityId="
				+ principleEntityId + ", Address=" + Address + ", Name=" + Name + "]";
	}

}
