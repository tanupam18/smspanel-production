package com.communications.tubelight.panel.sms.SmsPanel.Interface;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface FilesStorageService {
	public Map<String, Object> save(MultipartFile file, String username);

	public String UploadFileName();

	public String saveBlockNumber(MultipartFile file, String Username);

	public Map<String, Object> personalizeSave(MultipartFile file, String username);

	public String bulkUploadStorage(MultipartFile file, String username);

}