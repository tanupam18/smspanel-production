package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class DownloadApi {

	private String fileurl;
	private String status;

	public String getFileurl() {
		return fileurl;
	}

	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DownloadApi(String fileurl, String status) {
		super();
		this.fileurl = fileurl;
		this.status = status;
	}

	@Override
	public String toString() {
		return "DownloadApi [fileurl=" + fileurl + ", status=" + status + "]";
	}

}
