package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;

@Service
public class MobileLogsServices {

	private int noOfRecords;

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<SmsLogResponse> mobileLogs(int offset, int size, String search, String Date, String mobileNumber,
			String username) throws SQLException {
		System.out.println("offset---------------" + offset + "size-----------------------------" + size);
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;
		List<SmsLogResponse> list = null;
		String DBLogs = null;
		Timestamp MonthPendingSms = null;

		String DbLogsCustomerTable = "Select LogsDB from Main.Customers where  username = '" + username + "' limit 1;";

		try {
			resultSet = statement.executeQuery(DbLogsCustomerTable);
			if (resultSet.next())
				DBLogs = resultSet.getString("LogsDB");
			resultSet.close();

			String MonthonPendingSms = "SELECT SentLogTime FROM " + DBLogs + ".PendingSms where Username= '" + username
					+ "' \n" + "and SentLogTime Between '" + Date + " 00:00:00' And '" + Date
					+ " 23:59:59' AND Destination ='" + mobileNumber + "' limit 1 ";

			resultSet = statement.executeQuery(MonthonPendingSms);

			if (resultSet.next())
				MonthPendingSms = resultSet.getTimestamp("SentLogTime");
			resultSet.close();

			Calendar cal = Calendar.getInstance();
			String SmsTable =null;

			try {
				Format FormatDateString = new SimpleDateFormat("dd/MMMM/yyyy");

				FormatDateString = new SimpleDateFormat("yyyy");
				String SmsYear = FormatDateString.format(MonthPendingSms);

				FormatDateString = new SimpleDateFormat("MMM");
				String SmsMonth = FormatDateString.format(MonthPendingSms);

				SmsTable = DBLogs + ".Sms_" + SmsMonth + SmsYear;
				System.out.println(SmsTable);
				
			} catch (Exception e) {
				System.out.println(e);
			}
			
			

			String Sql;

			try {
				if (search != null && !search.isEmpty()) {

					Sql = "Select Source,Destination,MsgType,Message,MsgLength,MsgCount,UUID,DeliveryStatus,SentLogTime, DlrLoggedTime \n"
							+ "FROM tube_Logs.PendingSms where Username= '" + username + "' and SentLogTime Between '"
							+ Date + " 00:00:00' And '" + Date + " 23:59:59' \n" + "AND Destination = '" + mobileNumber
							+ "' AND (Message like '%" + search + "%' OR DeliveryStatus like '%" + search
							+ "%' OR SentLogTime like '%" + search + "%' OR DlrLoggedTime like '%" + search
							+ "%') UNION SELECT Source,Destination,MsgType,Message,MsgLength,MsgCount,\n"
							+ "UUID,DeliveryStatus,SentLogTime, DlrLoggedTime FROM "+SmsTable+" where Username= '"
							+ username + "' \n" + "and SentLogTime Between '" + Date + " 00:00:00' And '" + Date
							+ " 23:59:59' AND Destination = '" + mobileNumber + "'  AND (Message like '%" + search
							+ "%' OR DeliveryStatus like '%" + search + "%' OR SentLogTime like '%" + search
							+ "%' OR DlrLoggedTime like '%" + search + "%') limit " + "" + size;

				} else {
					Sql = "Select Source,Destination,MsgType,Message,MsgLength,MsgCount,UUID,DeliveryStatus,SentLogTime, DlrLoggedTime \n"
							+ "FROM " + DBLogs + ".PendingSms where Username= '" + username
							+ "' and SentLogTime Between '" + Date + " 00:00:00' And '" + Date + " 23:59:59' \n"
							+ "AND Destination = '" + mobileNumber
							+ "' UNION SELECT Source,Destination,MsgType,Message,MsgLength,MsgCount,\n"
							+ "UUID,DeliveryStatus,SentLogTime, DlrLoggedTime FROM " + SmsTable +" where Username= '" + username + "' \n" + "and SentLogTime Between '" + Date
							+ " 00:00:00' And '" + Date + " 23:59:59' AND Destination = '" + mobileNumber + "' limit  "
							+ offset + ", " + size;
				}

				System.out.println(Sql);
				resultSet = statement.executeQuery(Sql);

				list = new ArrayList<SmsLogResponse>();

				while (resultSet.next()) {

					SmsLogResponse logResponse = new SmsLogResponse();
					logResponse.setSource(resultSet.getString("source"));
					logResponse.setDestination(resultSet.getString("destination"));

					if (resultSet.getInt("msgtype") == 0) {
						logResponse.setSmsType("TEXT");
					} else {
						logResponse.setSmsType("UNICODE");

					}
					logResponse.setMessage(resultSet.getString("Message"));
					logResponse.setLength(resultSet.getInt("msglength"));
					logResponse.setCount(resultSet.getInt("msgcount"));
					logResponse.setId(resultSet.getString("uuid"));
					logResponse.setStatus(resultSet.getString("DeliveryStatus"));
					logResponse.setSubmitTime(resultSet.getString("SentLogTime"));
					logResponse.setDeliveryTime(resultSet.getString("DlrLoggedTime"));

					list.add(logResponse);
				}
				resultSet.close();
				
				resultSet = statement.executeQuery("Select Source,Destination,MsgType,Message,MsgLength,MsgCount,UUID,DeliveryStatus,SentLogTime, DlrLoggedTime \n"
						+ "FROM tube_Logs.PendingSms where Username= '"+username+"' and SentLogTime Between '"+Date+ " 00:00:00' And '"+Date+ " 23:59:59' \n"
						+ " UNION SELECT Source,Destination,MsgType,Message,MsgLength,MsgCount,\n"
						+ "UUID,DeliveryStatus,SentLogTime, DlrLoggedTime FROM "+SmsTable+" where Username= 'anupam2'");
				
				int count = 0;
				while(resultSet.next()) {
					count ++;
				}
				this.noOfRecords = count;
				
			

			} catch (Exception e) {
				System.out.println(e);
			}

		} catch (SQLException e) {
			System.out.println(e);
		}

		connection.close();
		return list;

	}
	
	public int getNoOfRecords() {
		return noOfRecords;
	}

}
