package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BalanceService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public String balanceService(String username) throws ClassNotFoundException, SQLException {

		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet resultSet;
		String balance = null;

		resultSet = statement.executeQuery("Select Balance from Customers where username='" + username + "'");

		if (resultSet.next()) {
			balance = resultSet.getString("Balance");
		}

		connection.close();

		return balance;
	}

}
