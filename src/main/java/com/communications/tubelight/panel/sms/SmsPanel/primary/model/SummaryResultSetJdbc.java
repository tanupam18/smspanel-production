package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class SummaryResultSetJdbc {

	private String DlrStatus;
	private String CountDlrStatus;
	private String month;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDlrStatus() {
		return DlrStatus;
	}

	public void setDlrStatus(String dlrStatus) {
		DlrStatus = dlrStatus;
	}

	public String getCountDlrStatus() {
		return CountDlrStatus;
	}

	public void setCountDlrStatus(String countDlrStatus) {
		CountDlrStatus = countDlrStatus;
	}

	public SummaryResultSetJdbc(String dlrStatus, String countDlrStatus, String month) {
		super();
		DlrStatus = dlrStatus;
		CountDlrStatus = countDlrStatus;
		this.month = month;
	}

	public SummaryResultSetJdbc() {
		super();
	}

	public SummaryResultSetJdbc(String dlrStatus2, String countDlrStatus2) {
	}

	@Override
	public String toString() {
		return "SummaryResultSetJdbc [DlrStatus=" + DlrStatus + ", CountDlrStatus=" + CountDlrStatus + ", month="
				+ month + "]";
	}

}
