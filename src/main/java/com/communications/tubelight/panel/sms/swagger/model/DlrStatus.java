package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class DlrStatus {

	private String PENDING;
	private String DELIVERD;
	private String REJECTD;
	private String UNDELIVRD;
	private String EXPIRED;

	public String getPENDING() {
		return PENDING;
	}

	public void setPENDING(String pENDING) {
		PENDING = pENDING;
	}

	public String getDELIVERD() {
		return DELIVERD;
	}

	public void setDELIVERD(String dELIVERD) {
		DELIVERD = dELIVERD;
	}

	public String getREJECTD() {
		return REJECTD;
	}

	public void setREJECTD(String rEJECTD) {
		REJECTD = rEJECTD;
	}

	public String getUNDELIVRD() {
		return UNDELIVRD;
	}

	public void setUNDELIVRD(String uNDELIVRD) {
		UNDELIVRD = uNDELIVRD;
	}

	public String getEXPIRED() {
		return EXPIRED;
	}

	public void setEXPIRED(String eXPIRED) {
		EXPIRED = eXPIRED;
	}

	public DlrStatus(String pENDING, String dELIVERD, String rEJECTD, String uNDELIVRD, String eXPIRED) {
		super();
		PENDING = pENDING;
		DELIVERD = dELIVERD;
		REJECTD = rEJECTD;
		UNDELIVRD = uNDELIVRD;
		EXPIRED = eXPIRED;
	}

	public DlrStatus() {
		super();
	}

	@Override
	public String toString() {
		return "DlrStatus [PENDING=" + PENDING + ", DELIVERD=" + DELIVERD + ", REJECTD=" + REJECTD + ", UNDELIVRD="
				+ UNDELIVRD + ", EXPIRED=" + EXPIRED + "]";
	}

}
