package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class uploadPersonalizeSwagger {
	
	private String valid;
	private String file;
	private String[] columns;
	private String faulty;
	private String duplicate;
	
	public String getValid() {
		return valid;
	}
	public void setValid(String valid) {
		this.valid = valid;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String[] getColumns() {
		return columns;
	}
	public void setColumns(String[] columns) {
		this.columns = columns;
	}
	public String getFaulty() {
		return faulty;
	}
	public void setFaulty(String faulty) {
		this.faulty = faulty;
	}
	public String getDuplicate() {
		return duplicate;
	}
	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
	
	public uploadPersonalizeSwagger(String valid, String file, String[] columns, String faulty, String duplicate) {
		super();
		this.valid = valid;
		this.file = file;
		this.columns = columns;
		this.faulty = faulty;
		this.duplicate = duplicate;
	}
	public uploadPersonalizeSwagger() {
		super();
	}
	
	
	
	

}
