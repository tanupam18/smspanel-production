package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class CountSmsResponse {

	private String username;
	private String sender;
	private String dlrStatus;
	private String countDlrStatus;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getDlrStatus() {
		return dlrStatus;
	}

	public void setDlrStatus(String dlrStatus) {
		this.dlrStatus = dlrStatus;
	}

	public String getCountDlrStatus() {
		return countDlrStatus;
	}

	public void setCountDlrStatus(String countDlrStatus) {
		this.countDlrStatus = countDlrStatus;
	}

	public CountSmsResponse(String username, String sender, String dlrStatus, String countDlrStatus) {
		super();
		this.username = username;
		this.sender = sender;
		this.dlrStatus = dlrStatus;
		this.countDlrStatus = countDlrStatus;
	}

	public CountSmsResponse() {
		super();
	}

	@Override
	public String toString() {
		return "CountSmsResponse [username=" + username + ", sender=" + sender + ", dlrStatus=" + dlrStatus
				+ ", countDlrStatus=" + countDlrStatus + "]";
	}

}
