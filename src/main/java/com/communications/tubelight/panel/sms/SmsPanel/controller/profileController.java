package com.communications.tubelight.panel.sms.SmsPanel.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.CreditLogModel;
import com.communications.tubelight.panel.sms.SmsPanel.response.AccountingResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.CreditLogResponse;
import com.communications.tubelight.panel.sms.SmsPanel.response.ProfileResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.AccountingupdateModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.ProfileModel;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.AccountingService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.CreditLoginService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.ProfileService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.UpdateAccountingService;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.UpdateProfileService;
import com.communications.tubelight.panel.sms.SmsPanel.security.JwtUtils;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/sms/api/v1")
@CrossOrigin
@Tag(name="9.Profile Controller API")
public class profileController {

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ProfileService profileService;

	@Autowired
	UpdateProfileService profileDetailService;

	@Autowired
	CreditLoginService creditLoginService;

	@Autowired
	AccountingService accountingService;

	@Autowired
	UpdateAccountingService updateAccountingService;

	@PostMapping("/profile")
	public Map profileDetail(@RequestHeader Map<String, String> headers) throws SQLException {
		
		System.out.println("profile_checking==========================");

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		try {
			profileService.profileService(Username);
		} catch (SQLException e) {
			System.out.println();
		}
		ProfileResponse ClientInfo = profileService.profileService(Username);

		Map<String, Object> AllClientnfo = new HashMap<String, Object>();

		Map<String, Object> AllCompanyInfo = new HashMap<String, Object>();

//		if (Username.equals("durgesh")) {
//			AllClientnfo.put("role", "admin");
//
//		} else {
//			AllClientnfo.put("role", "user");
//
//		}

		AllCompanyInfo.put("Company-Name", "Tubelight Communications Limited");
		AllCompanyInfo.put("Address",
				"Tubelight Communications Ltd. B-1702, Lotus Corporate Park, off western express highway, Goregaon (East),Mumbai- 400063");
		AllCompanyInfo.put("Support", "+91 8689899963 / 9811658716");
		AllCompanyInfo.put("Toll-free-number", "1800 123 0808");
		AllCompanyInfo.put("Website", "www.tubelightcommunications.com");

		AllClientnfo.put("client-profile", ClientInfo);
		AllClientnfo.put("company-profile", AllCompanyInfo);
		
		System.out.println("All_Client_info========================"+AllClientnfo);

		return AllClientnfo;

	}

	@PutMapping("/update/profile")
	public Map<String, Object> updateProfileDetail(@RequestHeader Map<String, String> headers,
			@RequestBody ProfileModel profileModel) {
//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		try {
			Map<String, Object> updateProfile = new HashMap<String, Object>();

			profileDetailService.updateProfileService(Username, profileModel.getCompanyName(),
					profileModel.getContactNo(), profileModel.getName(), profileModel.getPrincipleEntityId(),
					profileModel.getAddress());

			Map<String, Object> PauseOutput = new HashMap<String, Object>();
			PauseOutput.put("message", "Update User Profile successfully");
			return PauseOutput;

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Bad Gateway");
		}

	}

	@PostMapping("/accounting")
	public Map profileAccounting(@RequestHeader Map<String, String> headers) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		AccountingResponse a = accountingService.accountingService(Username);

		System.out.println(a);

		Map AllData = new HashMap();

		Map<String, String> AllUsername = new HashMap<String, String>();
		Map<String, String> AllMessageType = new HashMap<String, String>();
		Map<String, String> AccountType = new HashMap<String, String>();
		Map<String, String> AccountPassword = new HashMap<String, String>();
		Map<String, String> APIGenerate = new HashMap<String, String>();
		Map<String, String> CreditLogs = new HashMap<String, String>();

		AllUsername.put("name", "customerId");
		AllUsername.put("label", "Customer Id");

		AllMessageType.put("name", "messageType"); // Message Type
		AllMessageType.put("label", "Message Type");

		AccountType.put("name", "accountType");
		AccountType.put("label", "Account Type"); // Account Password

		AccountPassword.put("name", "Account Password");
		AccountPassword.put("label", "Account Password"); // Account Password

		APIGenerate.put("name", "API Generate ");
		APIGenerate.put("label", "API Generate ");

		CreditLogs.put("name", "Credit Logs");
		CreditLogs.put("label", "Credit Logs");

		List Alllist = new ArrayList();
		Alllist.add(AllUsername);
		Alllist.add(AllMessageType);
		Alllist.add(AccountType);
		Alllist.add(AccountPassword);
		Alllist.add(APIGenerate);
		Alllist.add(CreditLogs);

		AllData.put("data", a);
		AllData.put("fields", Alllist);

		return AllData;
	}

	@PutMapping("/accounting/update")
	public Map<String, String> profileAccountingUpdate(@RequestHeader Map<String, String> headers,
			@RequestBody AccountingupdateModel accountingupdateModel) {
//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}

		try {
			updateAccountingService.updateAccountingService(accountingupdateModel.getPassword(), Username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, String> AccountingPasswordUpdate = new HashMap<String, String>();
		AccountingPasswordUpdate.put("message", "Change Password successfully");

		return AccountingPasswordUpdate;

	}

	@PostMapping("/credit/logs")
	public Map CreditLogs(@RequestHeader Map<String, String> headers, @RequestBody CreditLogModel creditLogModel) {

//		String token = headers.get("authorization");
//		token = token.replaceAll("Bearer ", "");
//
//		String Username = jwtUtils.getUserNameFromJwtToken(token);
		
		String Username;
		try {
			String token = headers.get("authorization");
			token = token.replaceAll("Bearer ", "");
			Username = jwtUtils.getUserNameFromJwtToken(token);
			System.out.println("Username==========" + Username);

		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "");

		}


		List<CreditLogResponse> AllCreditLogs = null;
		try {
			AllCreditLogs = creditLoginService.creditLogService(
					(creditLogModel.getPage() - 1) * creditLogModel.getSize(), creditLogModel.getSize(),
					creditLogModel.getSearch(), Username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map CreditLogs = new HashMap();

		Map<String, Object> AllAdustedAt = new HashMap<String, Object>();
		Map<String, Object> AllUsername = new HashMap<String, Object>();
		Map<String, Object> AllNewCreditAlloted = new HashMap<String, Object>();
		Map<String, Object> AllOldCreditAlloted = new HashMap<String, Object>();
		Map<String, Object> AllNewBalance = new HashMap<String, Object>();
		Map<String, Object> AllOldBalance = new HashMap<String, Object>();
		Map<String, Object> AllAdjustment = new HashMap<String, Object>();

		int totalCount = creditLoginService.getNoOfRecords();

		List AddAllLabel = new ArrayList();

		AllAdustedAt.put("name", "adjustedAt");
		AllAdustedAt.put("label", "Adjusted At");

		//AllUsername.put("name", "username");
		//AllUsername.put("label", "Username");

		AllNewCreditAlloted.put("name", "newCreditAlloted");
		AllNewCreditAlloted.put("label", "New Credit Alloted"); // New Credit Alloted

		AllOldCreditAlloted.put("name", "oldCreditAlloted");
		AllOldCreditAlloted.put("label", "Old Credit Alloted");

		AllNewBalance.put("name", "newBalance");
		AllNewBalance.put("label", "New Balance");

		AllOldBalance.put("name", "oldBalance");
		AllOldBalance.put("label", "Old Balance");

		AllAdjustment.put("name", "adjustment");
		AllAdjustment.put("label", "Adjustment");

		AddAllLabel.add(AllAdustedAt);
		AddAllLabel.add(AllUsername);
		AddAllLabel.add(AllNewCreditAlloted);
		AddAllLabel.add(AllOldCreditAlloted);
		AddAllLabel.add(AllNewBalance);
		AddAllLabel.add(AllOldBalance);
		AddAllLabel.add(AllAdjustment);

		CreditLogs.put("data", AllCreditLogs);
		CreditLogs.put("totalCount", totalCount);
		CreditLogs.put("fields", AddAllLabel);

		return CreditLogs;

	}

}
