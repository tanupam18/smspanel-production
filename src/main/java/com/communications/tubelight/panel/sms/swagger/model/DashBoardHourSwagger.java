package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

import java.util.Arrays;

public class DashBoardHourSwagger {
	
	private String[] month;
	private Hour hour;
	private String[] weekly;
	
	public String[] getMonth() {
		return month;
	}
	public void setMonth(String[] month) {
		this.month = month;
	}
	public Hour getHour() {
		return hour;
	}
	public void setHour(Hour hour) {
		this.hour = hour;
	}
	public String[] getWeekly() {
		return weekly;
	}
	public void setWeekly(String[] weekly) {
		this.weekly = weekly;
	}
	public DashBoardHourSwagger(String[] month, Hour hour, String[] weekly) {
		super();
		this.month = month;
		this.hour = hour;
		this.weekly = weekly;
	}
	@Override
	public String toString() {
		return "DashBoardHourSwagger [month=" + Arrays.toString(month) + ", hour=" + hour + ", weekly="
				+ Arrays.toString(weekly) + "]";
	}
	
	
	
}

 

class  Hour {
	
	
}
