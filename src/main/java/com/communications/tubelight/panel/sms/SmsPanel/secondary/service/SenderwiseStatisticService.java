package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.SenderStatsticResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.ScheduleBulkJobResponse;

@Service
public class SenderwiseStatisticService {

	private int noOfRecords;

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public List<SenderStatsticResponse> senderwiseStatistic(int offset, int noOfRecords, String search, String username,
			String fromDate, String toDate) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		String Sql;
		List<SenderStatsticResponse> list = null;

		if (search != null && !search.isEmpty()) {
			Sql = "SELECT Source,DATE_FORMAT(`DateTime`,'%b%Y') AS SDate,SUM(Count) AS Submission, sum(if(DlrStatus = 'DELIVRD', Count, 0)) AS 'DELIVRD', \n"
					+ "sum(if(DlrStatus ='UNDELIV', Count, 0)) AS 'UNDELIV', sum(if(DlrStatus = 'EXPIRED', Count, 0)) AS 'EXPIRED', \n"
					+ "sum(if(DlrStatus = 'REJECTD', Count, 0)) AS 'REJECTD', sum(if(DlrStatus = 'DND', Count, 0)) AS 'DND',sum(if(DlrStatus = 'PENDING', Count, 0)) AS 'PENDING',( SUM(Count)- (sum(if(DlrStatus = 'DELIVRD', Count, 0)) + sum(if(DlrStatus ='UNDELIV', Count, 0)) \n"
					+ "+ sum(if(DlrStatus = 'EXPIRED', Count, 0)) + sum(if(DlrStatus = 'DND', Count, 0)) + sum(if(DlrStatus = 'REJECTD', Count, 0)) + sum(if(DlrStatus = 'PENDING', Count, 0)))) \n"
					+ "as Other, SUM(Price) AS Price ,  Source as SenderId FROM tube_Logs.Summary where Username='"
					+ username + "' and date(`DateTime`) BETWEEN '" + fromDate + "' \n" + "and  '" + toDate
					+ "' AND source like '%" + search + "%' GROUP BY Source limit " + noOfRecords;

		} else {
			Sql = "SELECT Source,DATE_FORMAT(`DateTime`,'%b%Y') AS SDate,SUM(Count) AS Submission, sum(if(DlrStatus = 'DELIVRD', Count, 0)) AS 'DELIVRD', \n"
					+ "sum(if(DlrStatus ='UNDELIV', Count, 0)) AS 'UNDELIV', sum(if(DlrStatus = 'EXPIRED', Count, 0)) AS 'EXPIRED', \n"
					+ "sum(if(DlrStatus = 'REJECTD', Count, 0)) AS 'REJECTD', sum(if(DlrStatus = 'DND', Count, 0)) AS 'DND',sum(if(DlrStatus = 'PENDING', Count, 0)) AS 'PENDING',( SUM(Count)- (sum(if(DlrStatus = 'DELIVRD', Count, 0)) + sum(if(DlrStatus ='UNDELIV', Count, 0)) \n"
					+ "+ sum(if(DlrStatus = 'EXPIRED', Count, 0)) + sum(if(DlrStatus = 'DND', Count, 0)) + sum(if(DlrStatus = 'REJECTD', Count, 0)) + sum(if(DlrStatus = 'PENDING', Count, 0)))) \n"
					+ "as Other, SUM(Price) AS Price ,  Source as SenderId FROM tube_Logs.Summary where Username='"+username +"' and date(`DateTime`) BETWEEN '"
					+ fromDate + "' \n" + "and  '" + toDate + "' GROUP BY Source limit " + offset + ", " + noOfRecords;
		}

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			System.out.println(e);
		}
		list = new ArrayList<SenderStatsticResponse>();

		try {
			while (resultSet.next()) {
				SenderStatsticResponse senderStatsticResponse = new SenderStatsticResponse();
				senderStatsticResponse.setSender(resultSet.getString("Source"));
				senderStatsticResponse.setSubmission(resultSet.getInt("Submission"));
				senderStatsticResponse.setDelivered(resultSet.getInt("DELIVRD"));
				senderStatsticResponse.setDnd(resultSet.getInt("DND"));
				senderStatsticResponse.setExpired(resultSet.getInt("EXPIRED"));
				senderStatsticResponse.setRejected(resultSet.getInt("REJECTD"));
				senderStatsticResponse.setUndelivered(resultSet.getInt("UNDELIV"));
				senderStatsticResponse.setPending(resultSet.getInt("PENDING"));
				senderStatsticResponse.setOthers(resultSet.getInt("Other"));

				list.add(senderStatsticResponse);

			}

			resultSet.close();

			resultSet = statement.executeQuery(
					"SELECT Source,DATE_FORMAT(`DateTime`,'%b%Y') AS SDate,SUM(Count) AS Submission, sum(if(DlrStatus = 'DELIVRD', Count, 0)) AS 'DELIVRD', \n"
							+ "sum(if(DlrStatus ='UNDELIV', Count, 0)) AS 'UNDELIV', sum(if(DlrStatus = 'EXPIRED', Count, 0)) AS 'EXPIRED', \n"
							+ "sum(if(DlrStatus = 'REJECTD', Count, 0)) AS 'REJECTD', sum(if(DlrStatus = 'DND', Count, 0)) AS 'DND',sum(if(DlrStatus = 'PENDING', Count, 0)) AS 'PENDING',( SUM(Count)- (sum(if(DlrStatus = 'DELIVRD', Count, 0)) + sum(if(DlrStatus ='UNDELIV', Count, 0)) \n"
							+ "+ sum(if(DlrStatus = 'EXPIRED', Count, 0)) + sum(if(DlrStatus = 'DND', Count, 0)) + sum(if(DlrStatus = 'REJECTD', Count, 0)) + sum(if(DlrStatus = 'PENDING', Count, 0)))) \n"
							+ "as Other, SUM(Price) AS Price ,  Source as SenderId FROM tube_Logs.Summary where Username='"+username+"'  GROUP BY Source ;\n"
							+ "");

			int count = 0;
			// System.out.println("resultset----------------------------------"+resultSet);

			while (resultSet.next()) {
				count++;
			}
			
			this.noOfRecords = count;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

	public int getNoOfRecords() {
		return this.noOfRecords;
	}
}
