package com.communications.tubelight.panel.sms.SmsPanel.response;

public class BulkTemplateResponse {

	private String template;
	private String TemplateName;
	private String sender;
	private String templateDltId;
	private String TemplateType;

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getTemplateName() {
		return TemplateName;
	}

	public void setTemplateName(String templateName) {
		TemplateName = templateName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTemplateDltId() {
		return templateDltId;
	}

	public void setTemplateDltId(String templateDltId) {
		this.templateDltId = templateDltId;
	}

	public String getTemplateType() {
		return TemplateType;
	}

	public void setTemplateType(String templateType) {
		TemplateType = templateType;
	}

	public BulkTemplateResponse(String template, String templateName, String sender, String templateDltId,
			String templateType) {
		super();
		this.template = template;
		TemplateName = templateName;
		this.sender = sender;
		this.templateDltId = templateDltId;
		TemplateType = templateType;
	}

	public BulkTemplateResponse() {
		super();
	}

	@Override
	public String toString() {
		return "BulkTemplateResponse [template=" + template + ", TemplateName=" + TemplateName + ", sender=" + sender
				+ ", templateDltId=" + templateDltId + ", TemplateType=" + TemplateType + "]";
	}

}
