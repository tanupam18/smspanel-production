package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.ManageSenderResponse;

@Service
public class ManageSenderService {

	private int noOfRecords;
	private int validationInsert;

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public List<ManageSenderResponse> manageSenderService(int offset, int size, String search, String sender,
			String username, boolean delete) throws SQLException {
		
		if (delete == true) {
			//statement.executeUpdate("Delete from UsersSenderId where Sender = '" + sender + "'");
			ManageSenderService manageSenderService = new ManageSenderService();
			manageSenderService.deleteManageSender(sender);

		}

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;

		Sql = "Select ProviderId from Customers where username='" + username + "'";

		String providerId = null;
		try {
			resultSet = statement.executeQuery(Sql);
			if (resultSet.next()) {
				providerId = resultSet.getString("ProviderId");
			}
			System.out.println("--------ProviderId-----------" + providerId);
		} catch (SQLException e) {
			System.out.println(e);
		}
		resultSet.close();

		// Statement statement =null;
		statement = connection.createStatement();
		if (search != null && !search.isEmpty()) {
			System.out.println("heyaheya");
		} else {
			if (delete == false && sender != null && !sender.isEmpty()) {
				String InsertSql = "INSERT INTO UsersSenderId(`Username`, `Sender`, `Template`, `ProviderId`, `EntityRelation`, `Peid`) VALUES ('"
						+ username + "', '" + sender + "', '" + sender + "', '" + providerId + "', '0', '')";
				System.out.println("InsertSql-------------------------" + InsertSql);

				this.validationInsert = statement.executeUpdate(InsertSql);

			}
		}
		String ManageSenderSql = null;

		if (search != null && !search.isEmpty()) {
			ManageSenderSql = "Select Username, Sender, AddTime, Status from Main.UsersSenderId where Username='"
					+ username + "' AND (Sender like '%" + search + "%' OR AddTime like '%" + search + "%') limit "
					+ size;

		} else {
			ManageSenderSql = "Select Username, Sender, AddTime, Status from UsersSenderId where Username='" + username
					+ "' limit " + offset + ", " + size;

		}
		System.out.println("ManageSenderSql-------------" + ManageSenderSql);

		List<ManageSenderResponse> AddManageSenderResponse = null;

		try {
			AddManageSenderResponse = new ArrayList<ManageSenderResponse>();

			resultSet = statement.executeQuery(ManageSenderSql);
			while (resultSet.next()) {
				ManageSenderResponse manageSenderResponse = new ManageSenderResponse();
				manageSenderResponse.setUsername(resultSet.getString("Username"));
				manageSenderResponse.setSender(resultSet.getString("Sender"));
				manageSenderResponse.setRequestedAt(resultSet.getString("AddTime"));
				if (resultSet.getInt("Status") == 2) {
					manageSenderResponse.setStatus("Pending");

				} else if (resultSet.getInt("Status") == 0) {
					manageSenderResponse.setStatus("Active");

				} else {
					manageSenderResponse.setStatus("DeActive");
				}

				AddManageSenderResponse.add(manageSenderResponse);

			}
			resultSet.close();

			String CountSql = "Select Count(*) as count from Main.UsersSenderId Where Username='" + username + "';";

			resultSet = statement.executeQuery(CountSql);

			if (resultSet.next()) {
				this.noOfRecords = resultSet.getInt("count");
			}

			resultSet.close();
		} catch (SQLException e) {
			System.out.println(e);
			System.out.println(e.getStackTrace()[0].getLineNumber());
		}
		// statement.close();
		connection.close();
		

		return AddManageSenderResponse;

	}

	public int getRecord() {
		return this.noOfRecords;
	}

	public void deleteManageSender(String sender) throws SQLException {
		String Local = "jdbc:mysql://localhost:3306/Main?useSSL=false&allowPublicKeyRetrieval=true&sessionVariables=sql_mode='NO_ENGINE_SUBSTITUTION'&jdbcCompliantTruncation=false";
		String UsernameConnection = "kannel";
		String Password = "sm@rTy51";

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;
		statement.executeUpdate("Delete from UsersSenderId where Sender = '" + sender + "'");
		connection.close();
		
		
	}
}
