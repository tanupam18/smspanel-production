package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class SingleSmsProcedure {

	private String Sender;
	private String mobileNo;
	private String MessageType;
	private String Messages;
	private Integer CountCharacter;
	private String PeId;
	private String TempId;

	public String getSender() {
		return Sender;
	}

	public void setSender(String sender) {
		Sender = sender;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessageType() {
		return MessageType;
	}

	public void setMessageType(String messageType) {
		MessageType = messageType;
	}

	public String getMessages() {
		return Messages;
	}

	public void setMessages(String messages) {
		Messages = messages;
	}

	public Integer getCountCharacter() {
		return CountCharacter;
	}

	public void setCountCharacter(Integer countCharacter) {
		CountCharacter = countCharacter;
	}

	public String getPeId() {
		return PeId;
	}

	public void setPeId(String peId) {
		PeId = peId;
	}

	public String getTempId() {
		return TempId;
	}

	public void setTempId(String tempId) {
		TempId = tempId;
	}

	public SingleSmsProcedure(String sender, String mobileNo, String messageType, String messages,
			Integer countCharacter, String peId, String tempId) {
		super();
		Sender = sender;
		this.mobileNo = mobileNo;
		MessageType = messageType;
		Messages = messages;
		CountCharacter = countCharacter;
		PeId = peId;
		TempId = tempId;
	}

	public SingleSmsProcedure() {
		super();
	}

	@Override
	public String toString() {
		return "SingleSmsProcedure [Sender=" + Sender + ", mobileNo=" + mobileNo + ", MessageType=" + MessageType
				+ ", Messages=" + Messages + ", CountCharacter=" + CountCharacter + ", PeId=" + PeId + ", TempId="
				+ TempId + "]";
	}

}
