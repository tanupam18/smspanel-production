package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class TemplateRetrievingRequest {

	private String search;

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public TemplateRetrievingRequest(String search) {
		super();
		this.search = search;
	}

	public TemplateRetrievingRequest() {
		super();
	}

	@Override
	public String toString() {
		return "TemplateRetrievingRequest [search=" + search + "]";
	}

}
