package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class DatestatisticModel {
	private int page;
	private int size;
	private String sender;
	private String search;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public DatestatisticModel(int page, int size, String sender, String search) {
		super();
		this.page = page;
		this.size = size;
		this.sender = sender;
		this.search = search;
	}

	public DatestatisticModel() {
		super();
	}

	@Override
	public String toString() {
		return "DatestatisticModel [page=" + page + ", size=" + size + ", sender=" + sender + ", search=" + search
				+ "]";
	}

}
