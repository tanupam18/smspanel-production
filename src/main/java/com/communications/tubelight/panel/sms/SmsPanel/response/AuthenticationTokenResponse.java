package com.communications.tubelight.panel.sms.SmsPanel.response;

public class AuthenticationTokenResponse {

	private String accessToken;
	private String refreshToken;
	private Integer expirydate;
	private String role;
	private String username;
	private String name;
	private String principleEntityId;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Integer getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(Integer expirydate) {
		this.expirydate = expirydate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrincipleEntityId() {
		return principleEntityId;
	}

	public void setPrincipleEntityId(String principleEntityId) {
		this.principleEntityId = principleEntityId;
	}

	public AuthenticationTokenResponse(String accessToken, String refreshToken, Integer expirydate, String role,
			String username, String name, String principleEntityId) {
		super();
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.expirydate = expirydate;
		this.role = role;
		this.username = username;
		this.name = name;
		this.principleEntityId = principleEntityId;
	}

	public AuthenticationTokenResponse(String accessToken, String role, String username, String name,
			String principleEntityId) {
		super();
		this.accessToken = accessToken;
		this.role = role;
		this.username = username;
		this.name = name;
		this.principleEntityId = principleEntityId;
	}

	@Override
	public String toString() {
		return "AuthenticationTokenResponse [accessToken=" + accessToken + ", refreshToken=" + refreshToken
				+ ", expirydate=" + expirydate + ", role=" + role + ", username=" + username + ", name=" + name
				+ ", CustomerDltId=" + principleEntityId + "]";
	}

	public AuthenticationTokenResponse() {
		super();
	}

}
