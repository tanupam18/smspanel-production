package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class RetreiveModelSwagger {

	private String username;
	private String template_info;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTemplate_info() {
		return template_info;
	}

	public void setTemplate_info(String template_info) {
		this.template_info = template_info;
	}

	public RetreiveModelSwagger(String username, String template_info) {
		super();
		this.username = username;
		this.template_info = template_info;
	}

	public RetreiveModelSwagger() {
		super();
	}

	@Override
	public String toString() {
		return "RetreiveModelSwagger [username=" + username + ", template_info=" + template_info + "]";
	}

}
