package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.entity.SummaryEntity;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.AllSummaryResultSetJdbc;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.repository.SummaryRepository;

@Service
public class SummaryService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

//	@Autowired
//	SummaryRepository summaryRepository;

	public ArrayList<SummaryResultSetJdbc> countSmsService(String sender, String Username, String fromDate, String toDate)
			throws SQLException, ClassNotFoundException {
		
		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet resultSet;
		ResultSet resultSet2;
		System.out.println("date--------------------"+fromDate);

		if (fromDate != null) {
			System.out.println("Adate----------------------"+fromDate);
			
			//Select DlrStatus, Count from Summary where DateTime between'2021-06-05' AND '2021-06-11';
			
			resultSet = statement.executeQuery(
					"Select Source, DlrStatus,Sum(Count) from Summary where Username='" + Username
							+ "' AND Source='" + sender + "' AND DateTime between '" + fromDate + "' AND '"+toDate+"'  group by DlrStatus");
			
			System.out.println("with_date and reseller--------------  "+ "Select resellername, DlrStatus,Sum(Count) from Summary where Source='" + sender
					+ "' AND Username='" + Username + "' AND DateTime='" + fromDate + "'  group by DlrStatus" );

		} else {
			resultSet = statement
					.executeQuery("Select Source, DlrStatus,Sum(Count) from Summary where Username='"
							+ Username + "' AND Source='" + sender + "' group by DlrStatus");
			System.out.println("without_date and reseller--------------"
					+ "Select resellername, DlrStatus,Sum(Count) from Summary where Resellername='" + sender
					+ "' AND Username='" + Username + "' group by DlrStatus");

		}

		String SenderName;
		String DlrStatus;
		String CountDlrStatus;
		String month;
		ArrayList<SummaryResultSetJdbc> SummaryResultSet = new ArrayList<>();

		while (resultSet.next()) {
//			SenderName = resultSet.getString("Source").trim();
			month="";
			DlrStatus = resultSet.getString("DlrStatus");
			CountDlrStatus = resultSet.getString("Sum(Count)");

			SummaryResultSetJdbc entity = new SummaryResultSetJdbc( DlrStatus, CountDlrStatus, month);
			SummaryResultSet.add(entity);

		}

		connection.close();
		return SummaryResultSet;
	}

	public ArrayList<AllSummaryResultSetJdbc> countAllSmsService(String Username, String fromDate, String toDate)
			throws SQLException, ClassNotFoundException {


		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet resultSet;
		System.out.println("-------------------------date2" + fromDate);


		if (fromDate != null) {
			resultSet = statement.executeQuery("Select DlrStatus,Sum(Count) from Summary Where Username='"
					+ Username + "' AND DateTime between '" + fromDate + "' AND '"+toDate+"' group by DlrStatus");

			System.out.println("All sender with date-----------  "+"Select DlrStatus,Sum(Count) from Summary Where Username='"
					+ Username + "' AND DateTime between '" + fromDate + "' AND '"+toDate+"' group by DlrStatus");

		} else {

			System.out.println("Select DlrStatus,Sum(Count) from Summary Where Username='" + Username
					+ "' group by DlrStatus");
			resultSet = statement.executeQuery("Select DlrStatus,SUM(Count) from Summary Where Username='"
					+ Username + "' group by DlrStatus");
			
		}

		String DlrStatus;
		String CountDlrStatus;

		ArrayList<AllSummaryResultSetJdbc> SummaryResultSet = new ArrayList<>();

		while (resultSet.next()) {
			DlrStatus = resultSet.getString("DlrStatus");
			CountDlrStatus = resultSet.getString("sum(Count)");
			// System.out.println("DlrStatus-----------------"+DlrStatus+"------------CountDlrStatus"+CountDlrStatus
			// );
			AllSummaryResultSetJdbc entity = new AllSummaryResultSetJdbc(DlrStatus, CountDlrStatus);
//			System.out.println("--------------------------"+entity);
			SummaryResultSet.add(entity);

		}

		connection.close();

		return SummaryResultSet;

	}

	public String countMultipleSmsService(String sender, String Username, String date) {

		return null;

	}

}
