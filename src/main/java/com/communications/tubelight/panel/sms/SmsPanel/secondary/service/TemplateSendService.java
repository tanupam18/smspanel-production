package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SendTemplateModel;

@Service
public class TemplateSendService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public int sendTemplateService(SendTemplateModel sendTemplateModel, String username) throws SQLException {

		System.out.println("Local--------" + Local + "   UsernameConnection------" + UsernameConnection
				+ "Password-------------" + Password);

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {

		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {

		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {

		}

		String sql1 = "Select count(*) from Template  where Template='" + sendTemplateModel.getMessage()
				+ "' and username='" + username + "' and SenderId='" + sendTemplateModel.getSender()
				+ "' and TemplateDltId='" + sendTemplateModel.getDltTemplateId() + "'";
		int count;
		System.out.println("-------Select Query--------" + sql1);

		ResultSet resultSet;

		resultSet = statement.executeQuery(sql1);
		resultSet.next();
		count = resultSet.getInt(1);

		if (count == 0) {
			String sql = "Insert Into Template(Username,TemplateDltId, SenderId, Ttype, TempName, Template)Values('"
					+ username + "', '" + sendTemplateModel.getDltTemplateId() + "','" + sendTemplateModel.getSender()
					+ "','" + sendTemplateModel.getMessageType() + "','" + sendTemplateModel.getTemplateName()
					+ "',trim('" + sendTemplateModel.getMessage() + "'))";

			System.out.println("Insertion Query---------------" + sql);

			try {
				statement.executeUpdate(sql);
			} catch (SQLException e) { //messageType
				throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Message Type must be text or unicode");

			}
		}

		try {
			connection.close();
		} catch (SQLException e) {
		}
		return count;

	}

}
