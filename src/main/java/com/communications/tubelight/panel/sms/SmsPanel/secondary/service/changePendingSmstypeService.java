package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class changePendingSmstypeService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public Map<String, String> getCustomerPendingSms(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ResultSet resultSet;

		String sql = "Select LogsDB, server from  Main.Customers  where username='"+username+"'";
		
		Map<String, String> mapCustomer =null;
		try {
			resultSet = statement.executeQuery(sql);
			String logsDb =null;
			String server=null;
			
			 mapCustomer = new HashMap<String, String>();
			
			while (resultSet.next()) { 
				
				 logsDb = resultSet.getString("LogsDB");
				 server = resultSet.getString("server");
			}
			mapCustomer.put("logsdb", logsDb);
			mapCustomer.put("server", server);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapCustomer;

	}

}
