package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;

@Service
public class SummaryWeeklyService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<Object> SummaryWeekMonth(String Username)
			throws ClassNotFoundException, SQLException {

		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet monthlyDashBoard;
		ResultSet resultSet2;

		monthlyDashBoard = statement
				.executeQuery("Select DateTime, DlrStatus, SUM(Count) FROM Summary WHERE Username = '" + Username
						+ "'AND DateTime BETWEEN (CURDATE() - INTERVAL 6 DAY) AND CURDATE() GROUP BY DateTime, DlrStatus");

		SummaryResultSetJdbc entity = null;
		Map<String, HashMap<String, String>> weekmap  = new HashMap<String, HashMap<String, String>>();
		List<Object> WeeklyArray = new ArrayList<>();

		while (monthlyDashBoard.next()) {
			 
			String week = monthlyDashBoard.getString("DateTime");
			String DlrStatus = monthlyDashBoard.getString("DlrStatus");
			String CountDlrStatus = monthlyDashBoard.getString("Sum(Count)");
			
			if (weekmap.containsKey(week)) {
				weekmap.get(week).put(DlrStatus, CountDlrStatus);

			} else {
				HashMap<String, String> dlrMap = new HashMap<>();
				dlrMap.put(DlrStatus, CountDlrStatus);
				weekmap.put(week, dlrMap);

			}
			
			
			//weekmap.put("DateTime", week);
			//weekmap.put(DlrStatus, CountDlrStatus);
			

			entity = new SummaryResultSetJdbc(DlrStatus, CountDlrStatus, week);

		}
		WeeklyArray.add(weekmap);
		connection.close();

		return WeeklyArray;

	}

}
