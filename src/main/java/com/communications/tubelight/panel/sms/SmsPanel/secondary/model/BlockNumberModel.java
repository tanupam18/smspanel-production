package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class BlockNumberModel {
	private int page;
	private int size;
	private String search;
	private String mobileNo;
	private boolean delete;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getMobileno() {
		return mobileNo;
	}

	public void setMobileno(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public BlockNumberModel(int page, int size, String search, String mobileno, boolean delete) {
		super();
		this.page = page;
		this.size = size;
		this.search = search;
		this.mobileNo = mobileno;
		this.delete = delete;
	}

	public BlockNumberModel() {
		super();
	}

	@Override
	public String toString() {
		return "BlockNumberModel [page=" + page + ", size=" + size + ", search=" + search + ", mobileno=" + mobileNo
				+ ", delete=" + delete + "]";
	}

}
