package com.communications.tubelight.panel.sms.SmsPanel.secondary.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Summary")
public class SummaryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String Username;
	private String Resellername;
	private String Gateway;
	private String OGateway;
	private String Source;
	private String DlrStatus;
	private String ErrorCode;
	private String Circle;
	private String Operator;

	private int JobId;
	private int ActivityCount;
	private int Count;
	private BigDecimal price;
	private BigDecimal GatewayPrice;
	private int SentHour;

	private Timestamp DateTIme;
	private String DBLogs;
	private String ProcessServer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getResellername() {
		return Resellername;
	}

	public void setResellername(String resellername) {
		Resellername = resellername;
	}

	public String getGateway() {
		return Gateway;
	}

	public void setGateway(String gateway) {
		Gateway = gateway;
	}

	public String getOGateway() {
		return OGateway;
	}

	public void setOGateway(String oGateway) {
		OGateway = oGateway;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getDlrStatus() {
		return DlrStatus;
	}

	public void setDlrStatus(String dlrStatus) {
		DlrStatus = dlrStatus;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public String getCircle() {
		return Circle;
	}

	public void setCircle(String circle) {
		Circle = circle;
	}

	public String getOperator() {
		return Operator;
	}

	public void setOperator(String operator) {
		Operator = operator;
	}

	public int getJobId() {
		return JobId;
	}

	public void setJobId(int jobId) {
		JobId = jobId;
	}

	public int getActivityCount() {
		return ActivityCount;
	}

	public void setActivityCount(int activityCount) {
		ActivityCount = activityCount;
	}

	public int getCount() {
		return Count;
	}

	public void setCount(int count) {
		Count = count;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getGatewayPrice() {
		return GatewayPrice;
	}

	public void setGatewayPrice(BigDecimal gatewayPrice) {
		GatewayPrice = gatewayPrice;
	}

	public int getSentHour() {
		return SentHour;
	}

	public void setSentHour(int sentHour) {
		SentHour = sentHour;
	}

	public Timestamp getDateTIme() {
		return DateTIme;
	}

	public void setDateTIme(Timestamp dateTIme) {
		DateTIme = dateTIme;
	}

	public String getDBLogs() {
		return DBLogs;
	}

	public void setDBLogs(String dBLogs) {
		DBLogs = dBLogs;
	}

	public String getProcessServer() {
		return ProcessServer;
	}

	public void setProcessServer(String processServer) {
		ProcessServer = processServer;
	}

	@Override
	public String toString() {
		return "Summary [id=" + id + ", Username=" + Username + ", Resellername=" + Resellername + ", Gateway="
				+ Gateway + ", OGateway=" + OGateway + ", Source=" + Source + ", DlrStatus=" + DlrStatus
				+ ", ErrorCode=" + ErrorCode + ", Circle=" + Circle + ", Operator=" + Operator + ", JobId=" + JobId
				+ ", ActivityCount=" + ActivityCount + ", Count=" + Count + ", price=" + price + ", GatewayPrice="
				+ GatewayPrice + ", SentHour=" + SentHour + ", DateTIme=" + DateTIme + ", DBLogs=" + DBLogs
				+ ", ProcessServer=" + ProcessServer + "]";
	}

	public SummaryEntity(Long id, String username, String resellername, String gateway, String oGateway, String source,
			String dlrStatus, String errorCode, String circle, String operator, int jobId, int activityCount, int count,
			BigDecimal price, BigDecimal gatewayPrice, int sentHour, Timestamp dateTIme, String dBLogs,
			String processServer) {
		super();
		this.id = id;
		Username = username;
		Resellername = resellername;
		Gateway = gateway;
		OGateway = oGateway;
		Source = source;
		DlrStatus = dlrStatus;
		ErrorCode = errorCode;
		Circle = circle;
		Operator = operator;
		JobId = jobId;
		ActivityCount = activityCount;
		Count = count;
		this.price = price;
		GatewayPrice = gatewayPrice;
		SentHour = sentHour;
		DateTIme = dateTIme;
		DBLogs = dBLogs;
		ProcessServer = processServer;
	}

	public SummaryEntity() {
		super();
	}

}
