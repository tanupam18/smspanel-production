package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

import java.util.Arrays;
import java.util.List;

public class MobileLogs {

	private List<Datas> data;
	private String totalCount;
	private String[] fields;

	public List<Datas> getData() {
		return data;
	}

	public void setData(List<Datas> data) {
		this.data = data;
	}

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public MobileLogs(List<Datas> data, String totalCount, String[] fields) {
		super();
		this.data = data;
		this.totalCount = totalCount;
		this.fields = fields;
	}

	@Override
	public String toString() {
		return "MobileLogs [data=" + data + ", totalCount=" + totalCount + ", fields=" + Arrays.toString(fields) + "]";
	}

}

class Datas {

	private String source;
	private String destination;
	private String type;
	private String message;
	private String length;
	private String count;
	private String submitTime;
	private String deliveryTime;
	private String id;
	private String status;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Datas(String source, String destination, String type, String message, String length, String count,
			String submitTime, String deliveryTime, String id, String status) {
		super();
		this.source = source;
		this.destination = destination;
		this.type = type;
		this.message = message;
		this.length = length;
		this.count = count;
		this.submitTime = submitTime;
		this.deliveryTime = deliveryTime;
		this.id = id;
		this.status = status;
	}

	@Override
	public String toString() {
		return "Datas [source=" + source + ", destination=" + destination + ", type=" + type + ", message=" + message
				+ ", length=" + length + ", count=" + count + ", submitTime=" + submitTime + ", deliveryTime="
				+ deliveryTime + ", id=" + id + ", status=" + status + "]";
	}

}
