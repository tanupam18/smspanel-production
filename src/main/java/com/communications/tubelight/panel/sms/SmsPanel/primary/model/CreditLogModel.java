package com.communications.tubelight.panel.sms.SmsPanel.primary.model;

public class CreditLogModel {
	private int Page;
	private int Size;
	private String search;

	public int getPage() {
		return Page;
	}

	public void setPage(int page) {
		Page = page;
	}

	public int getSize() {
		return Size;
	}

	public void setSize(int size) {
		Size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public CreditLogModel(int page, int size, String search) {
		super();
		Page = page;
		Size = size;
		this.search = search;
	}

	public CreditLogModel() {
		super();
	}

	@Override
	public String toString() {
		return "CreditLogModel [Page=" + Page + ", Size=" + Size + ", search=" + search + "]";
	}

}
