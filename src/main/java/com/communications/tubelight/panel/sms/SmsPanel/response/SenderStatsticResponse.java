package com.communications.tubelight.panel.sms.SmsPanel.response;

public class SenderStatsticResponse {

	// private String date;
	private String Sender;
	private int Submission;
	private int delivered;
	private int undelivered;
	private int expired;
	private int rejected;
	private int dnd;
	private int pending;
	private int others;

	public String getSender() {
		return Sender;
	}

	public void setSender(String sender) {
		Sender = sender;
	}

	public int getSubmission() {
		return Submission;
	}

	public void setSubmission(int submission) {
		Submission = submission;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public int getUndelivered() {
		return undelivered;
	}

	public void setUndelivered(int undelivered) {
		this.undelivered = undelivered;
	}

	public int getExpired() {
		return expired;
	}

	public void setExpired(int expired) {
		this.expired = expired;
	}

	public int getRejected() {
		return rejected;
	}

	public void setRejected(int rejected) {
		this.rejected = rejected;
	}

	public int getDnd() {
		return dnd;
	}

	public void setDnd(int dnd) {
		this.dnd = dnd;
	}

	public int getPending() {
		return pending;
	}

	public void setPending(int pending) {
		this.pending = pending;
	}

	public int getOthers() {
		return others;
	}

	public void setOthers(int others) {
		this.others = others;
	}

	public SenderStatsticResponse(String sender, int submission, int delivered, int undelivered, int expired,
			int rejected, int dnd, int pending, int others) {
		super();
		Sender = sender;
		Submission = submission;
		this.delivered = delivered;
		this.undelivered = undelivered;
		this.expired = expired;
		this.rejected = rejected;
		this.dnd = dnd;
		this.pending = pending;
		this.others = others;
	}

	public SenderStatsticResponse() {
		super();
	}

	@Override
	public String toString() {
		return "SenderStatsticResponse [Sender=" + Sender + ", Submission=" + Submission + ", delivered=" + delivered
				+ ", undelivered=" + undelivered + ", expired=" + expired + ", rejected=" + rejected + ", dnd=" + dnd
				+ ", pending=" + pending + ", others=" + others + "]";
	}

}
