package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class SenderWiseStatisticModel {

	private long fromDate;
	private long toDate;
	private int page;
	private int size;
	private String search;

	public long getFromDate() {
		return fromDate;
	}

	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}

	public long getToDate() {
		return toDate;
	}

	public void setToDate(long toDate) {
		this.toDate = toDate;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public SenderWiseStatisticModel(long fromDate, long toDate, int page, int size, String search) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.page = page;
		this.size = size;
		this.search = search;
	}

	public SenderWiseStatisticModel() {
		super();
	}

	@Override
	public String toString() {
		return "SenderWiseStatisticModel [fromDate=" + fromDate + ", toDate=" + toDate + ", page=" + page + ", size="
				+ size + ", search=" + search + "]";
	}

}
