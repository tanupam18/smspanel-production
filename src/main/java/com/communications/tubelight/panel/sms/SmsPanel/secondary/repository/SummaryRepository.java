package com.communications.tubelight.panel.sms.SmsPanel.secondary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.entity.SummaryEntity;

@Repository
public interface SummaryRepository extends JpaRepository<SummaryEntity, Long> {

//	@Query(value="select username from users where id =:id" , nativeQuery = true)
//	String findNameById(@Param("id") Long id);
	
	@Query(value="Select resellername, DlrStatus,count(DlrStatus) from Summary where Resellername=:reseller AND Username=:username group by DlrStatus" , nativeQuery = true)
	SummaryEntity findCountSMs(@Param("username") String username, @Param("reseller") String reseller);
	
}
