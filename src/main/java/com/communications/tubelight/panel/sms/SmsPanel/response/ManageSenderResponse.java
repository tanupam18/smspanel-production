package com.communications.tubelight.panel.sms.SmsPanel.response;

public class ManageSenderResponse {

	private String username;
	private String sender;
	private String RequestedAt;
	private String status;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRequestedAt() {
		return RequestedAt;
	}

	public void setRequestedAt(String requestedAt) {
		RequestedAt = requestedAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ManageSenderResponse(String username, String sender, String requestedAt, String status) {
		super();
		this.username = username;
		this.sender = sender;
		RequestedAt = requestedAt;
		this.status = status;
	}

	public ManageSenderResponse() {
		super();
	}

	@Override
	public String toString() {
		return "ManageSenderResponse [username=" + username + ", sender=" + sender + ", RequestedAt=" + RequestedAt
				+ ", status=" + status + "]";
	}

}
