package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.JobSummaryResponse;

@Service
public class JobSummaryServiceDownload {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<JobSummaryResponse> jobSummary(String username) {

		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;

		List<JobSummaryResponse> list = null;
		String Sql;

		Sql = "Select u.QueuedAt AS queued, u.JobId, Filename, Message, TotalNumbers, sum(CASE WHEN s.DlrStatus = 'DELIVRD' THEN s.Count else 0 END) as Delivrd FROM Main.UserJobs AS u, tube_Logs.Summary AS s WHERE s.Username = '"
				+ username + "' and " + "s.JobId = u.JobId  GROUP BY s.JobId ORDER BY QueuedAt";

		System.out.println(Sql);
		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		list = new ArrayList<JobSummaryResponse>();

		try {
			while (resultSet.next()) {
				JobSummaryResponse jobSummaryResponse = new JobSummaryResponse();

				jobSummaryResponse.setDate(resultSet.getString("queued"));
				jobSummaryResponse.setFile(resultSet.getString("Filename"));
				jobSummaryResponse.setDelivered(resultSet.getInt("Delivrd"));
				jobSummaryResponse.setJobid(resultSet.getInt("u.JobId"));
				jobSummaryResponse.setSms(resultSet.getString("Message"));
				jobSummaryResponse.setTotal(resultSet.getInt("TotalNumbers"));
				list.add(jobSummaryResponse);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return list;

	}

}
