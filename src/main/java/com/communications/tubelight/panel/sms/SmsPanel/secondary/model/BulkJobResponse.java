package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BulkJobResponse {

	private Long Summary;
	private String FileName;
	private String type;
	private String message;
	private int length;
	private String sender;
	private int Tcount;
	private String QueuedAt;
	private String CompletedAt;
	private int Tsent;
	private int Status;
	private String StatusType;

	public String getStatusType() {
		return StatusType;
	}

	public void setStatusType(String statusType) {
		StatusType = statusType;
	}

	public Long getSummary() {
		return Summary;
	}

	public void setSummary(Long summary) {
		Summary = summary;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public int getTcount() {
		return Tcount;
	}

	public void setTcount(int tcount) {
		Tcount = tcount;
	}

	public String getQueuedAt() {
		return QueuedAt;
	}

	public void setQueuedAt(String queuedAt) {
		QueuedAt = queuedAt;
	}

	public String getCompletedAt() {
		return CompletedAt;
	}

	public void setCompletedAt(String completedAt) {
		CompletedAt = completedAt;
	}

	public int getTsent() {
		return Tsent;
	}

	public void setTsent(int tsent) {
		Tsent = tsent;
	}
	@JsonIgnore
	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}

	public BulkJobResponse(Long summary, String fileName, String type, String message, int length, String sender,
			int tcount, String queuedAt, String completedAt, int tsent, int status, String statusType) {
		super();
		Summary = summary;
		FileName = fileName;
		this.type = type;
		this.message = message;
		this.length = length;
		this.sender = sender;
		Tcount = tcount;
		QueuedAt = queuedAt;
		CompletedAt = completedAt;
		Tsent = tsent;
		Status = status;
		StatusType = statusType;
	}

	public BulkJobResponse() {
		super();
	}

	@Override
	public String toString() {
		return "BulkJobResponse [Summary=" + Summary + ", FileName=" + FileName + ", type=" + type + ", message="
				+ message + ", length=" + length + ", sender=" + sender + ", Tcount=" + Tcount + ", QueuedAt="
				+ QueuedAt + ", CompletedAt=" + CompletedAt + ", Tsent=" + Tsent + ", Status=" + Status
				+ ", StatusType=" + StatusType + "]";
	}

}
