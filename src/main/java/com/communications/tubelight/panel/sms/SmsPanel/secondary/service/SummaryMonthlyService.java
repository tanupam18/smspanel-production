package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.primary.model.SummaryResultSetJdbc;

@Service
public class SummaryMonthlyService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public List<Object> SummaryWeekMonth(String Username) throws ClassNotFoundException, SQLException {

		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(Local, UsernameConnection, Password);

		Statement statement;
		statement = connection.createStatement();
		ResultSet monthlyDashBoard;
		ResultSet resultSet2;

		monthlyDashBoard = statement
				.executeQuery("Select MONTH(DateTime) as month, DlrStatus , SUM(Count) from Summary WHERE Username = '"
						+ Username + "' AND DateTime BETWEEN (CURDATE() - INTERVAL 5 MONTH) AND CURDATE() "
						+ "GROUP BY MONTH(DateTime), DlrStatus");
System.out.println("Select MONTH(DateTime) as month, DlrStatus , SUM(Count) from Summary WHERE Username = '"
						+ Username + "' AND DateTime BETWEEN (CURDATE() - INTERVAL 5 MONTH) AND CURDATE() "
						+ "GROUP BY MONTH(DateTime), DlrStatus");
		SummaryResultSetJdbc entity = null;

		Map<String, HashMap<String, String>> monthlyMap =new HashMap<>();
		List<Object> MonthlyArray = new ArrayList<>();
		
		while (monthlyDashBoard.next()) {
			
			String month = monthlyDashBoard.getString("month");
			String DlrStatus = monthlyDashBoard.getString("DlrStatus");
			String CountDlrStatus = monthlyDashBoard.getString("Sum(Count)");
			if(monthlyMap.containsKey(month)) {
				monthlyMap.get(month).put(DlrStatus, CountDlrStatus);
				
			}else {
				HashMap<String, String> dlrMap = new HashMap<>();
				dlrMap.put(DlrStatus, CountDlrStatus);
				monthlyMap.put(month, dlrMap);

			}
			//MonthlyArray.add(monthlyMap);

			

		}
		MonthlyArray.add(monthlyMap);
 
		connection.close();

		return MonthlyArray;

	}

}
