package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class BlockRetreivingMultipleNumberModel {
	private int page;
	private int size;
	private String Search;
	private boolean delete;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return Search;
	}

	public void setSearch(String search) {
		Search = search;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public BlockRetreivingMultipleNumberModel(int page, int size, String search, boolean delete) {
		super();
		this.page = page;
		this.size = size;
		Search = search;
		this.delete = delete;
	}

	public BlockRetreivingMultipleNumberModel() {
		super();
	}

	@Override
	public String toString() {
		return "BlockRetreivingMultipleNumberModel [page=" + page + ", size=" + size + ", Search=" + Search
				+ ", delete=" + delete + "]";
	}

}
