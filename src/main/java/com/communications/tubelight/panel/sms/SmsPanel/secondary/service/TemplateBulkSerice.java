package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.response.BulkTemplateResponse;

@Service
public class TemplateBulkSerice {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	public void templateBulkService(BulkTemplateResponse bulkTemplateResponse, String username) throws SQLException {

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		String Sql;
		ResultSet resultSet = null;

		System.out.println(bulkTemplateResponse.getSender() + "-----------------" + bulkTemplateResponse.getTemplate()
				+ "----------" + bulkTemplateResponse.getTemplateDltId() + "---------------"
				+ bulkTemplateResponse.getTemplateName());

		String InsertSql = "INSERT INTO `Main`.`Template` (`Username`, `SenderId`, `Ttype`, `TempName`, `TemplateDltId`, `Template`) VALUES ('"
				+ username + "', '" + bulkTemplateResponse.getSender() + "', '" + bulkTemplateResponse.getTemplateType()
				+ "', '" + bulkTemplateResponse.getTemplateName() + "', '" + bulkTemplateResponse.getTemplateDltId()
				+ "', '" + bulkTemplateResponse.getTemplateName() + "')";
		//
		try {
			statement.executeUpdate(InsertSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			connection.close();
		}
		

	}

}
