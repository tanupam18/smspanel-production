package com.communications.tubelight.panel.sms.SmsPanel.response;

public class AccountingResponse {

	private String CustomerId;
	private String MessageType;
	private String AccountType;

	public String getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}

	public String getMessageType() {
		return MessageType;
	}

	public void setMessageType(String messageType) {
		MessageType = messageType;
	}

	public String getAccountType() {
		return AccountType;
	}

	public void setAccountType(String accountType) {
		AccountType = accountType;
	}

	public AccountingResponse(String customerId, String messageType, String accountType) {
		super();
		CustomerId = customerId;
		MessageType = messageType;
		AccountType = accountType;
	}

	public AccountingResponse() {
		super();
	}

	@Override
	public String toString() {
		return "AccountingResponse [CustomerId=" + CustomerId + ", MessageType=" + MessageType + ", AccountType="
				+ AccountType + "]";
	}

}
