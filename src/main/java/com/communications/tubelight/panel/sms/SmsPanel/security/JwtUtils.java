package com.communications.tubelight.panel.sms.SmsPanel.security;



import java.util.Date;



import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Component;



import com.communications.tubelight.panel.sms.SmsPanel.primary.entity.User;

import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.UserRepository;

import com.communications.tubelight.panel.sms.SmsPanel.primary.repository.*;



//import com.communications.tubelight.api.messaging.entity.User;

//import com.communications.tubelight.api.messaging.repository.UserRepository;



import io.jsonwebtoken.*;



@Component

public class JwtUtils {



	@Value("${spring.secret.key}")

	private String SecretKey;

	

	@Autowired

	UserRepositoryService userRepository;



//	@Autowired

//	UserRepository userRepository;

	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);



//	public String getProductBySecretKey(Long urlById) {

//		User exisiting = userRepository.findById(urlById).orElse(null);

//		return exisiting.getSecretkey();

//	}



	public String generateTokenFromUsername(String username, long expirationDate, String ip, String createdAt) {

		userRepository.loginInsertion(username, ip, createdAt);

		

		System.out.println("username----------------"+username);

		return Jwts.builder().setSubject(username).setIssuedAt(new Date())

				.setExpiration(new Date((new Date()).getTime() + expirationDate))

				.signWith(SignatureAlgorithm.HS512, SecretKey).compact(); // SignatureAlgorithm.HS512, secretkey

	}



	public String getUserNameFromJwtToken(String token) {

		return Jwts.parser().setSigningKey(SecretKey).parseClaimsJws(token).getBody().getSubject();

	}



	public boolean validateJwtToken(String authToken) {

		try {



			Jwts.parser().setSigningKey(SecretKey).parseClaimsJws(authToken);

			return true;

		} catch (SignatureException e) {

			logger.error("Invalid JWT signature: {}", e.getMessage());

		} catch (MalformedJwtException e) {

			logger.error("Invalid JWT token: {}", e.getMessage());

		} catch (ExpiredJwtException e) {

			logger.error("JWT token is expired: {}", e.getMessage());

		} catch (UnsupportedJwtException e) {

			logger.error("JWT token is unsupported: {}", e.getMessage());

		} catch (IllegalArgumentException e) {

			logger.error("JWT claims string is empty: {}", e.getMessage());

		}

		return false;

	}

}
