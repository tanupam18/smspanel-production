package com.communications.tubelight.panel.sms.SmsPanel.secondary.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MatchTemplateService {

	@Value("${spring.datasource.first.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.first.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.first.password}")
	private String Password;

	public String matchTemplate(String message, String username, String sender) throws SQLException {

		Connection connection = null;
		String CountReturnRow = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ResultSet resultSet;

		try {
			message = message.trim();
			message.replace("'", "\\'");
			message.replace("\"", "\"");
			
			System.out.println("Select Template, TemplateDltId from Template where Template='" + message
					+ "' and username='" + username + "' and SenderId='" + sender + "'  limit 1");

			resultSet = statement.executeQuery("Select Template, TemplateDltId from Template where Template='" + message
					+ "' and username='" + username + "' and SenderId='" + sender + "'  limit 1");

			String template;
			String TemplateDltId;
			int count;

			resultSet.next();
			CountReturnRow = resultSet.getString(2);
			System.out.println("-----COuntReturnRow----------------------------" + CountReturnRow);

		} catch (SQLException e) {
			System.out.println(e);
		}
		connection.close();

		return CountReturnRow;

	}
}
