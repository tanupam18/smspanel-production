package com.communications.tubelight.panel.sms.SmsPanel.response;

public class JobSummaryResponse {

	private String date;
	private int jobid;
	private String file;
	private String sms;
	private int total;
	private int delivered;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getJobid() {
		return jobid;
	}

	public void setJobid(int jobid) {
		this.jobid = jobid;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public JobSummaryResponse(String date, int jobid, String file, String sms, int total, int delivered) {
		super();
		this.date = date;
		this.jobid = jobid;
		this.file = file;
		this.sms = sms;
		this.total = total;
		this.delivered = delivered;
	}

	public JobSummaryResponse() {
		super();
	}

	@Override
	public String toString() {
		return "JobSummaryResponse [date=" + date + ", jobid=" + jobid + ", file=" + file + ", sms=" + sms + ", total="
				+ total + ", delivered=" + delivered + "]";
	}

}
