package com.communications.tubelight.panel.sms.SmsPanel.secondary.service.download;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.communications.tubelight.panel.sms.SmsPanel.secondary.model.SmsLogResponse;
import com.communications.tubelight.panel.sms.SmsPanel.secondary.service.DownlooadCsvService;

@Service
public class SmsLogDownloadService {

	@Value("${spring.datasource.second.jdbcUrl}")
	private String Local;

	@Value("${spring.datasource.second.username}")
	private String UsernameConnection;

	@Value("${spring.datasource.second.password}")
	private String Password;

	@Autowired
	DownlooadCsvService downlooadCsvService;

	public List<SmsLogResponse> smsLogDownload(String username) {

		Connection connection = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(Local, UsernameConnection, Password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet resultSet = null;
		List<SmsLogResponse> list = null;

		String Sql;

		Sql = "Select source, destination, msgtype, Message, msglength, msgcount,uuid, DeliveryStatus,smscSubmitdate, SmscDonedate from PendingSms WHERE username = '"
				+ username + "' order by smscSubmitdate Asc";

		try {
			resultSet = statement.executeQuery(Sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		list = new ArrayList<SmsLogResponse>();

		try {
			while (resultSet.next()) {

				SmsLogResponse logResponse = new SmsLogResponse();
				logResponse.setSource(resultSet.getString("source"));
				logResponse.setDestination(resultSet.getString("destination"));
				if (resultSet.getInt("msgtype") == 0) {
					logResponse.setSmsType("TEXT");

				} else {
					logResponse.setSmsType("UNICODE");

				}
				
				logResponse.setMessage(URLDecoder.decode(resultSet.getString("Message")));
				logResponse.setLength(resultSet.getInt("msglength"));
				logResponse.setCount(resultSet.getInt("msgcount"));
				logResponse.setId(resultSet.getString("uuid"));
				logResponse.setStatus(resultSet.getString("DeliveryStatus"));
				logResponse.setSubmitTime(resultSet.getString("smscSubmitdate"));
				logResponse.setDeliveryTime(resultSet.getString("SmscDonedate"));

				list.add(logResponse);
				//downlooadCsvService.smsLogServiceDownload(list);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;

	}

}
