package com.communications.tubelight.panel.sms.SmsPanel.secondary.model;

public class RetreiveDownloadModel {

	private int page;
	private int size;
	private String Search;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getSearch() {
		return Search;
	}

	public void setSearch(String search) {
		Search = search;
	}

	public RetreiveDownloadModel(int page, int size, String search) {
		super();
		this.page = page;
		this.size = size;
		Search = search;
	}

	@Override
	public String toString() {
		return "RetreiveDownloadModel [page=" + page + ", size=" + size + ", Search=" + Search + "]";
	}

	public RetreiveDownloadModel() {
		super();
	}

}
