package com.communications.tubelight.panel.sms.SmsPanel.swagger.model;

public class uploadSwagger {
	
	private String valid;
	private String file;
	private String faulty;
	private String duplicate;
	
	public String getValid() {
		return valid;
	}
	public void setValid(String valid) {
		this.valid = valid;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getFaulty() {
		return faulty;
	}
	public void setFaulty(String faulty) {
		this.faulty = faulty;
	}
	public String getDuplicate() {
		return duplicate;
	}
	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}
	public uploadSwagger(String valid, String file, String faulty, String duplicate) {
		super();
		this.valid = valid;
		this.file = file;
		this.faulty = faulty;
		this.duplicate = duplicate;
	}
	
	
	
	public uploadSwagger() {
		super();
	}
	@Override
	public String toString() {
		return "uploadSwagger [valid=" + valid + ", file=" + file + ", faulty=" + faulty + ", duplicate=" + duplicate
				+ "]";
	}
	
	
	

}
